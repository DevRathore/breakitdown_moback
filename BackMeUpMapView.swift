//
//  BackMeUpMapView.swift
//  BackMeUp
//
//  Created by Focus's WorkStation on 7/23/15.
//  Copyright (c) 2015 MoBack Inc. All rights reserved.
//

import UIKit
import MapKit

class BackMeUpMapView: MKMapView, MKMapViewDelegate {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.delegate = self
        self.mapType = MKMapType.standard
        self.isPitchEnabled = false
        self.isRotateEnabled = false
        
        
        // setting visible area
        let initialLocation = CLLocation(latitude: 37.7648334, longitude: -122.3903216)
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(initialLocation.coordinate,
            regionRadius * 1.5, regionRadius * 1.5)
        self.setRegion(coordinateRegion, animated: true)
    
        let annotation = BackMeUpAnnotation(title: "UCSF Benioff Children's Hospital",
            locationName: "1975 4th St, San Francisco, CA 94158",
            coordinate: CLLocationCoordinate2D(latitude: 37.7648334, longitude: -122.3903216))
        self.addAnnotation(annotation)
    }
    
    // MARK: - MKMapViewDelegate
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView! {
        let standardAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "MyCustomAnnotation")
        standardAnnotationView.animatesDrop = true
        standardAnnotationView.canShowCallout = true
        standardAnnotationView.rightCalloutAccessoryView = UIButton(type: .detailDisclosure) as UIView
        return standardAnnotationView
    }
}

class BackMeUpAnnotation: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.coordinate = coordinate
        
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
}


