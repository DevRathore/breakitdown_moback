//
//  HomeViewController.swift
//  BreakItDown
//
//  Created by Dobango on 05/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var menuOptionView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func homeAction(_ sender: Any) {
    }
    
    
    @IBAction func navigateTabViewAction(_ sender: UIButton) {
        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController")
//        self.present(vc!, animated: true, completion: nil)
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewController") as? ViewController
        vc?.indexNumber = sender.tag
                
        /*
        var tabs = [HHTabButton]()
        
        for index in 0...4 {
            let tab = HHTabButton(withTitle: titles[index], tabImage: icons[index], index: index)
            tab.titleLabel?.font = tabFont
            tab.setHHTabBackgroundColor(color: defaultTabColor, forState: .normal)
            tab.setHHTabBackgroundColor(color: selectedTabColor, forState: .selected)
            tab.imageToTitleSpacing = spacing
            tab.imageVerticalAlignment = .top
            tab.imageHorizontalAlignment = .center
            tabs.append(tab)
        }
        
        vc?.hhTabBarView.tabBarTabs = tabs

        
        vc?.hhTabBarView.tabBarTabs[1].setHHTabBackgroundColor(color: Color.Blue.Medium, forState: .normal)
         vc?.hhTabBarView.tabBarTabs[1].setHHTabBackgroundColor(color: Color.Orange.Medium, forState: .selected)
        */
        
        self.present(vc!, animated: false) {
            
        }
    }
    
    @IBAction func editNoteAction(_ sender: UIButton) {
     
        if sender.isSelected == false
        {
            sender.isSelected = true
            menuOptionView.isHidden = false
            
        }
        else{
            sender.isSelected = false
            menuOptionView.isHidden = true
        }
    }
    
    @IBAction func faqAction(_ sender: Any) {
        
        menuButton.isSelected = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FaqViewController") as? FaqViewController
        self.present(vc!, animated: false) {
            
        }

        
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        viewFaq.layer.add(transition, forKey: "animation")
        self.viewFaq.isHidden = false
        */

    }
    
    @IBAction func notesAction(_ sender: Any) {
        
        menuButton.isSelected = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NotesViewController") as? NotesViewController
        self.present(vc!, animated: false) {
            
        }
        
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        notesView.layer.add(transition, forKey: "animation")
        self.notesView.isHidden = false
        notesTextView.text = UserDefaults.standard.object(forKey: "notes") as? String
        */
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
