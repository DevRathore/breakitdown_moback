//
//  ClavicleViewController.swift
//  BreakItDown
//
//  Created by Dobango on 08/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit

class ClavicleViewController: UIViewController {

 
    @IBOutlet weak var anatomyViewContainer: UIView!
    
    @IBOutlet weak var fractureViewContainer: UIView!
    
    @IBOutlet weak var treatmentViewContainer: UIView!
    
    @IBOutlet weak var recoveryViewContainer: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        anatomyViewContainer.isHidden = false
        fractureViewContainer.isHidden = true
        treatmentViewContainer.isHidden = true
        recoveryViewContainer.isHidden = true
    }

    @IBAction func anatomyAction(_ sender: Any) {
        anatomyViewContainer.isHidden = false
        fractureViewContainer.isHidden = true
        treatmentViewContainer.isHidden = true
        recoveryViewContainer.isHidden = true
    }
    
    @IBAction func fractureAction(_ sender: Any) {
        anatomyViewContainer.isHidden = true
        fractureViewContainer.isHidden = false
        treatmentViewContainer.isHidden = true
        recoveryViewContainer.isHidden = true
    }
    
    @IBAction func treatmentAction(_ sender: Any) {
        anatomyViewContainer.isHidden = true
        fractureViewContainer.isHidden = true
        treatmentViewContainer.isHidden = false
        recoveryViewContainer.isHidden = true
    }
    
    @IBAction func recoveryAction(_ sender: Any) {
        anatomyViewContainer.isHidden = true
        fractureViewContainer.isHidden = true
        treatmentViewContainer.isHidden = true
        recoveryViewContainer.isHidden = false
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
