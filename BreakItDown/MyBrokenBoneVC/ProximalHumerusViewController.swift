//
//  ProximalHumerusViewController.swift
//  BreakItDown
//
//  Created by Dobango on 08/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit

class ProximalHumerusViewController: UIViewController {

    @IBOutlet weak var proximalViewContainer: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    @IBAction func anatomyAction(_ sender: Any) {
    }
    
    @IBAction func fractureAction(_ sender: Any) {
    }
    
    @IBAction func treatmentAction(_ sender: Any) {
    }
    
    @IBAction func recoveryAction(_ sender: Any) {
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
