//
//  NotesViewController.swift
//  BreakItDown
//
//  Created by Dobango on 21/11/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit

class NotesViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var txtView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        txtView.delegate = self
        txtView.text = UserDefaults.standard.object(forKey: "notes") as? String
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.dismiss(animated: false) {
        }
        UserDefaults.standard.set(txtView.text, forKey: "notes")
    }
    
    @IBAction func homeAction(_ sender: Any) {
        
        UserDefaults.standard.set(txtView.text, forKey: "notes")
        self.dismiss(animated: false) {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "removeTabbarView"), object: nil)
        }
    }

    
    @IBAction func closeAction(_ sender: Any) {
        
        self.dismiss(animated: false) {
        }
        UserDefaults.standard.set(txtView.text, forKey: "notes")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
