//
//  FiveViewController.swift
//  BreakItDown
//
//  Created by Dobango on 05/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit
import Firebase

class FiveViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    @IBOutlet weak var tableViewList: UITableView!
    var myCareTeamArrayCount = 2
    
    var addingCell:Bool = false
    var selectedIndex: Int?

    @IBOutlet weak var menuOptionView: UIView!
    var isSelect: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        
        // Do any additional setup after loading the view.
        
        tableViewList.register(UINib(nibName: CellIdentifiers.anatomyCell1, bundle: nil), forCellReuseIdentifier: CellIdentifiers.anatomyCell1)
        
        tableViewList.estimatedRowHeight = 100
        tableViewList.rowHeight = UITableViewAutomaticDimension
        
        //------------ BoneHealthNew-------------------------------------
        
//        FirebaseApp.configure()

        let refBoneHealth = Database.database().reference().child("BONEHEALTHNEW")
        refBoneHealth.observe(DataEventType.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                for objectData in snapshot.children.allObjects as! [DataSnapshot] {
                    //getting values
                    let objectValue = objectData.value as? [String: AnyObject]
                    
                    let objectContent1: String  = objectValue?["fractureHealing"] as! String
                    let objectContent2: String  = objectValue?["fracturePrevention"] as! String
                    
                    localDataClavicle.fractureHealingDetails = objectContent1
                    localDataClavicle.fracturePreventionDetails = objectContent2

                }
            }
        })
        
        self.reloadContents()
        
        //---------------------------------------------------------------------
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return myCareTeamArrayCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var celll:UITableViewCell?
        
        if indexPath.row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
            cell.selectionStyle = .none
            cell.updateCell("Fracture Healing", color1: Color.Purple.Medium, color2: Color.Purple.Light)
            if addingCell && selectedIndex == indexPath.row {
                cell.data.isHidden = false
                cell.data.text = localDataClavicle.fractureHealingDetails
                cell.contentView.setNeedsLayout()
            } else {
                cell.data.isHidden = true
                cell.data.text = ""
                cell.contentView.setNeedsLayout()
            }
            
            return cell
        }
        else if indexPath.row == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
            cell.selectionStyle = .none
            cell.updateCell("Fracture Prevention", color1: Color.Purple.Medium, color2: Color.Purple.Light)
            if addingCell && selectedIndex == indexPath.row {
                cell.data.isHidden = false
                cell.data.text = localDataClavicle.fracturePreventionDetails
                cell.contentView.setNeedsLayout()
            } else {
                cell.data.isHidden = true
                cell.data.text = ""
                cell.contentView.setNeedsLayout()
            }
            
            
            return cell
        }
        
        return celll!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UIDevice.current.userInterfaceIdiom != .pad {
            
            if addingCell == false
            {
                addingCell = true
            }
            else{
                addingCell = false
            }
            tableView.reloadData()
            selectedIndex = indexPath.row
            scrollTableViewToRow(indexPath.row)
        }
    }
    
    func scrollTableViewToRow(_ row:Int) {
        let seconds = 0.1
        let delay = seconds * Double(NSEC_PER_SEC)  // nanoseconds per seconds
        let dispatchTime = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.tableViewList.scrollToRow(at: IndexPath(row: row, section: 0), at: UITableViewScrollPosition.top, animated: true)
        })
        
    }
    
    
    func reloadContents() {
        tableViewList.delegate = self
        tableViewList.dataSource = self
        tableViewList.reloadData()
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "removeTabbarView"), object: nil)
    }
    
    
    @IBAction func backToHome(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "removeTabbarView"), object: nil)
    }

    @IBAction func menuAction(_ sender: Any) {
        
        if isSelect == false
        {
            isSelect = true
            menuOptionView.isHidden = false
        }
        else{
            isSelect = false
            menuOptionView.isHidden = true
        }
    }
 
    
    @IBAction func faqAction(_ sender: UIButton) {
        
        isSelect = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FaqViewController") as? FaqViewController
        self.present(vc!, animated: false) {
            
        }
        
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        viewFaq.layer.add(transition, forKey: "animation")
        self.viewFaq.isHidden = false
        */
    }
    
    @IBAction func notesAction(_ sender: Any) {
        
        isSelect = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NotesViewController") as? NotesViewController
        self.present(vc!, animated: false) {
            
        }
        
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        notesView.layer.add(transition, forKey: "animation")
        self.notesView.isHidden = false
        
        notesTextView.text = UserDefaults.standard.object(forKey: "notes") as? String
        */
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
