//
//  FirstDetailViewController.swift
//  BreakItDown
//
//  Created by Dobango on 08/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit
import Firebase

class FirstDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    @IBOutlet weak var tableViewList: UITableView!
    @IBOutlet weak var lblBoneType: UILabel!
    
    @IBOutlet weak var anatomyBtn: UIButton!
    @IBOutlet weak var fractureBtn: UIButton!
    @IBOutlet weak var treatmentBtn: UIButton!
    @IBOutlet weak var recoveryBtn: UIButton!
    
    @IBOutlet weak var menuOptionView: UIView!
    
    var textPopUpView = UITextView()
    
    var isSelect: Bool = false
    
    var tabType = ""
    var innerTabType = ""
    
    var addingCell:Bool = false
    var expandedCellName = ""

    var clavicleAnatomyArrayCount = 3
    var clavicleFractureArrayCount = 3
    var clavicleTreatmentArrayCount = 3
    var clavicleRecoveryArrayCount = 2

    var proximalAnatomyArrayCount = 3
    var proximalFractureArrayCount = 3
    var proximalTreatmentArrayCount = 3
    var proximalRecoveryArrayCount = 1
    
    var selectedIndex: Int?

    var dictionaryOfArrayData: [[String:String]] = [["header":"", "summary": "abc", "image": "", "detail":"", "presscription":""], ["header":"", "summary": "", "image": "abc", "detail":"", "presscription":""], ["header":"", "summary": "", "image": "", "detail":"", "presscription":"abc"], ["header":"Non Operative", "summary": "", "image": "", "detail":"", "presscription":""], ["header":"Operative", "summary": "", "image": "", "detail":"", "presscription":""]]

    var arrayData: [String:String] = ["header":"", "summary": "", "image": "", "detail":"abc", "presscription":""]

    var tapRecognizer = UITapGestureRecognizer()
    
    override func viewDidAppear(_ animated: Bool) {
        
//        FirebaseApp.configure()

        if tabType == "Clavicle" {
            //------------ ClavicleNew --------------------------------------------
            let refCalvicle = Database.database().reference().child("CLAVICLENEW")
            refCalvicle.observe(DataEventType.value, with: { (snapshot) in
                if snapshot.childrenCount > 0 {
                    for objectData in snapshot.children.allObjects as! [DataSnapshot] {
                        //getting values
                        let objectValue = objectData.value as? [String: AnyObject]
                        let objectTitle: String = objectValue?["title"] as! String
                        print("objectTitle --->>> ",objectTitle);
                        
                        let objectContent: String  = objectValue?["content"] as! String
                        let objectContentType: String  = objectValue?["type"] as! String
                        
                        let objectSubtitle: [String: AnyObject]  = objectValue?["subtitle"] as! [String : AnyObject]
                        
                        if (objectTitle == "anatomy")
                        {
                            localDataClavicle.clavicleAnatomy = objectContent
                            localDataClavicle.clavicleAnatomyType = objectContentType
                            
                        }
                        else if (objectTitle == "fracture")
                        {
                            localDataClavicle.clavicleFracture = objectContent
                            localDataClavicle.clavicleFractureType = objectContentType
                        }
                        else if (objectTitle == "treatment")
                        {
                            localDataClavicle.clavicleTreatment = objectContent
                            if objectSubtitle["Non-operative Treatment"] as? String != nil
                            {
                                localDataClavicle.clavicleNonOperativeTreatment = objectSubtitle["Non-operative Treatment"] as! String
                            }
                            
                            if objectSubtitle["Operative Treatment"] as? String != nil
                            {
                                localDataClavicle.clavicleOperativeTreatment = objectSubtitle["Operative Treatment"] as! String
                            }
                            
                        }
                        else if (objectTitle == "recovery")
                        {
                            localDataClavicle.clavicleRecovery = objectContent
                            
                             localDataClavicle.clavicleNonOperativeRecovery = objectSubtitle["Non-operative Recovery"] as! String
                             localDataClavicle.clavicleOperativeRecovery = objectSubtitle["Operative Recovery"] as! String
                            
                        }
                    }
                }
            })
        }
        else if tabType == "Proximal Humerus" {
            
            //------------ ElbowNew --------------------------------------------
            let refProximal = Database.database().reference().child("PROXIMALNEW")
            refProximal.observe(DataEventType.value, with: { (snapshot) in
                if snapshot.childrenCount > 0 {
                    for objectData in snapshot.children.allObjects as! [DataSnapshot] {
                        //getting values
                        let objectValue = objectData.value as? [String: AnyObject]
                        let objectTitle: String = objectValue?["title"] as! String
                        print("objectTitle --->>> ",objectTitle);
                        
                        let objectContent: String  = objectValue?["content"] as! String
                        
                        let objectSubtitle: [String: AnyObject]  = objectValue?["subtitle"] as! [String : AnyObject]
                        
                        if (objectTitle == "anatomy")
                        {
                            let objectContentType: String  = objectValue?["type"] as! String
                            
                            localDataClavicle.proximalAnatomy = objectContent
                            localDataClavicle.proximalAnatomyType = objectContentType
                            
                        }
                        else if (objectTitle == "fracture")
                        {
                            let objectContentType: [String:String]  = objectValue?["type"] as! [String:String]
                            
                            localDataClavicle.proximalFracture = objectContent
                            localDataClavicle.proximalFractureType1 = objectContentType["type1"]!
                            localDataClavicle.proximalFractureType2 = objectContentType["type2"]!
                            
                        }
                        else if (objectTitle == "treatment")
                        {
                            localDataClavicle.proximalTreatment = objectContent
                            localDataClavicle.clavicleNonOperativeTreatment = objectSubtitle["Non-operative"] as! String
                            localDataClavicle.clavicleOperativeTreatment = objectSubtitle["Operative"] as! String
                        }
                        else if (objectTitle == "recovery")
                        {
                            localDataClavicle.proximalRecovery = objectContent
                            /*
                             localDataClavicle.clavicleRecovery = objectSubtitle["Non-operative Recovery"] as! String
                             localDataClavicle.clavicleRecovery = objectSubtitle["Operative Recovery"] as! String
                             */
                        }
                    }
                }
            })
            //---------------------------------------------------------------------
        }
        if tabType == "Leg" {
            
            //------------ LegNew --------------------------------------------
            let refLeg = Database.database().reference().child("LEGNEW")
            refLeg.observe(DataEventType.value, with: { (snapshot) in
                if snapshot.childrenCount > 0 {
                    for objectData in snapshot.children.allObjects as! [DataSnapshot] {
                        //getting values
                        let objectValue = objectData.value as? [String: AnyObject]
                        let objectTitle: String = objectValue?["title"] as! String
                        print("objectTitle --->>> ",objectTitle);
                        
                        let objectContent: String  = objectValue?["content"] as! String
                        let objectContentType: String  = objectValue?["type"] as! String
                        
                        let objectSubtitle: [String: AnyObject]  = objectValue?["subtitle"] as! [String : AnyObject]
                        
                        if (objectTitle == "anatomy")
                        {
                            localDataClavicle.LegAnatomy = objectContent
                            //                        localDataClavicle.clavicleAnatomyType = objectContentType
                            
                        }
                        else if (objectTitle == "fracture")
                        {
                            localDataClavicle.LegFracture = objectContent
                            //                        localDataClavicle.clavicleFractureType = objectContentType
                        }
                        else if (objectTitle == "treatment")
                        {
                            localDataClavicle.LegTreatment = objectContent
                            //                        localDataClavicle.clavicleNonOperativeTreatment = objectSubtitle["Non-operative Treatment"] as! String
                            //                        localDataClavicle.clavicleOperativeTreatment = objectSubtitle["Operative Treatment"] as! String
                        }
                        else if (objectTitle == "recovery")
                        {
                            localDataClavicle.LegRecovery = objectContent
                            /*
                             localDataClavicle.clavicleRecovery = objectSubtitle["Non-operative Recovery"] as! String
                             localDataClavicle.clavicleRecovery = objectSubtitle["Operative Recovery"] as! String
                             */
                        }
                    }
                }
            })
            //---------------------------------------------------------------------
        }
        else if tabType == "Wrist" {
            
            //------------ WristNew --------------------------------------------
            let refWrist = Database.database().reference().child("WRISTNEW")
            refWrist.observe(DataEventType.value, with: { (snapshot) in
                if snapshot.childrenCount > 0 {
                    for objectData in snapshot.children.allObjects as! [DataSnapshot] {
                        //getting values
                        let objectValue = objectData.value as? [String: AnyObject]
                        let objectTitle: String = objectValue?["title"] as! String
                        print("objectTitle --->>> ",objectTitle);
                        
                        let objectContent: String  = objectValue?["content"] as! String
                        let objectContentType: String  = objectValue?["type"] as! String
                        
                        let objectSubtitle: [String: AnyObject]  = objectValue?["subtitle"] as! [String : AnyObject]
                        
                        if (objectTitle == "anatomy")
                        {
                            localDataClavicle.WristAnatomy = objectContent
                            //                        localDataClavicle.clavicleAnatomyType = objectContentType
                            
                        }
                        else if (objectTitle == "fracture")
                        {
                            localDataClavicle.WristFracture = objectContent
                            //                        localDataClavicle.clavicleFractureType = objectContentType
                        }
                        else if (objectTitle == "treatment")
                        {
                            localDataClavicle.WristTreatment = objectContent
                            //                        localDataClavicle.clavicleNonOperativeTreatment = objectSubtitle["Non-operative Treatment"] as! String
                            //                        localDataClavicle.clavicleOperativeTreatment = objectSubtitle["Operative Treatment"] as! String
                        }
                        else if (objectTitle == "recovery")
                        {
                            localDataClavicle.WristRecovery = objectContent
                            /*
                             localDataClavicle.clavicleRecovery = objectSubtitle["Non-operative Recovery"] as! String
                             localDataClavicle.clavicleRecovery = objectSubtitle["Operative Recovery"] as! String
                             */
                        }
                    }
                }
            })
            //---------------------------------------------------------------------
        }
        if tabType == "Ankle" {
         
            //------------ AnkleNew --------------------------------------------
            let refAnkle = Database.database().reference().child("ANKLENEW")
            refAnkle.observe(DataEventType.value, with: { (snapshot) in
                if snapshot.childrenCount > 0 {
                    for objectData in snapshot.children.allObjects as! [DataSnapshot] {
                        //getting values
                        let objectValue = objectData.value as? [String: AnyObject]
                        let objectTitle: String = objectValue?["title"] as! String
                        print("objectTitle --->>> ",objectTitle);
                        
                        let objectContent: String  = objectValue?["content"] as! String
                        let objectContentType: String  = objectValue?["type"] as! String
                        
                        let objectSubtitle: [String: AnyObject]  = objectValue?["subtitle"] as! [String : AnyObject]
                        
                        if (objectTitle == "anatomy")
                        {
                            localDataClavicle.AnkleAnatomy = objectContent
                            //                        localDataClavicle.clavicleAnatomyType = objectContentType
                            
                        }
                        else if (objectTitle == "fracture")
                        {
                            localDataClavicle.AnkleFracture = objectContent
                            //                        localDataClavicle.clavicleFractureType = objectContentType
                        }
                        else if (objectTitle == "treatment")
                        {
                            localDataClavicle.AnkleTreatment = objectContent
                            //                        localDataClavicle.clavicleNonOperativeTreatment = objectSubtitle["Non-operative Treatment"] as! String
                            //                        localDataClavicle.clavicleOperativeTreatment = objectSubtitle["Operative Treatment"] as! String
                        }
                        else if (objectTitle == "recovery")
                        {
                            localDataClavicle.AnkleRecovery = objectContent
                            /*
                             localDataClavicle.clavicleRecovery = objectSubtitle["Non-operative Recovery"] as! String
                             localDataClavicle.clavicleRecovery = objectSubtitle["Operative Recovery"] as! String
                             */
                        }
                    }
                }
            })
            //---------------------------------------------------------------------
        }
        else if tabType == "Forearm" {
            
            //------------ ForearmNew --------------------------------------------
            
            let refForearm = Database.database().reference().child("FOREARM")
            refForearm.observe(DataEventType.value, with: { (snapshot) in
                if snapshot.childrenCount > 0 {
                    for objectData in snapshot.children.allObjects as! [DataSnapshot] {
                        //getting values
                        let objectValue = objectData.value as? [String: AnyObject]
                        let objectTitle: String = objectValue?["title"] as! String
                        print("objectTitle --->>> ",objectTitle);
                        
                        let objectContent: String  = objectValue?["content"] as! String
                        let objectContentType: String  = objectValue?["type"] as! String
                        
                        let objectSubtitle: [String: AnyObject]  = objectValue?["subtitle"] as! [String : AnyObject]
                        
                        if (objectTitle == "anatomy")
                        {
                            localDataClavicle.foreArmAnatomy = objectContent
                            //                        localDataClavicle.clavicleAnatomyType = objectContentType
                            
                        }
                        else if (objectTitle == "fracture")
                        {
                            localDataClavicle.foreArmFracture = objectContent
                            //                        localDataClavicle.clavicleFractureType = objectContentType
                        }
                        else if (objectTitle == "treatment")
                        {
                            localDataClavicle.foreArmTreatment = objectContent
                            //                        localDataClavicle.clavicleNonOperativeTreatment = objectSubtitle["Non-operative Treatment"] as! String
                            //                        localDataClavicle.clavicleOperativeTreatment = objectSubtitle["Operative Treatment"] as! String
                        }
                        else if (objectTitle == "recovery")
                        {
                            localDataClavicle.foreArmRecovery = objectContent
                            /*
                             localDataClavicle.clavicleRecovery = objectSubtitle["Non-operative Recovery"] as! String
                             localDataClavicle.clavicleRecovery = objectSubtitle["Operative Recovery"] as! String
                             */
                        }
                    }
                }
            })
            
            //---------------------------------------------------------------------
        }
        if tabType == "Femur" {
            
            //------------ FemurNew --------------------------------------------
            let refFemur = Database.database().reference().child("FEMURNEW")
            refFemur.observe(DataEventType.value, with: { (snapshot) in
                if snapshot.childrenCount > 0 {
                    for objectData in snapshot.children.allObjects as! [DataSnapshot] {
                        //getting values
                        let objectValue = objectData.value as? [String: AnyObject]
                        let objectTitle: String = objectValue?["title"] as! String
                        print("objectTitle --->>> ",objectTitle);
                        
                        let objectContent: String  = objectValue?["content"] as! String
                        let objectContentType: String  = objectValue?["type"] as! String
                        
                        let objectSubtitle: [String: AnyObject]  = objectValue?["subtitle"] as! [String : AnyObject]
                        
                        if (objectTitle == "anatomy")
                        {
                            localDataClavicle.FemurAnatomy = objectContent
                            //                        localDataClavicle.clavicleAnatomyType = objectContentType
                            
                        }
                        else if (objectTitle == "fracture")
                        {
                            localDataClavicle.FemurFracture = objectContent
                            //                        localDataClavicle.clavicleFractureType = objectContentType
                        }
                        else if (objectTitle == "treatment")
                        {
                            localDataClavicle.FemurTreatment = objectContent
                            //                        localDataClavicle.clavicleNonOperativeTreatment = objectSubtitle["Non-operative Treatment"] as! String
                            //                        localDataClavicle.clavicleOperativeTreatment = objectSubtitle["Operative Treatment"] as! String
                        }
                        else if (objectTitle == "recovery")
                        {
                            localDataClavicle.FemurRecovery = objectContent
                            /*
                             localDataClavicle.clavicleRecovery = objectSubtitle["Non-operative Recovery"] as! String
                             localDataClavicle.clavicleRecovery = objectSubtitle["Operative Recovery"] as! String
                             */
                        }
                    }
                }
            })
            //---------------------------------------------------------------------
        }
        if tabType == "Knee" {
            
            //------------ KneeNew --------------------------------------------
            let refKnee = Database.database().reference().child("KNEENEW")
            refKnee.observe(DataEventType.value, with: { (snapshot) in
                if snapshot.childrenCount > 0 {
                    for objectData in snapshot.children.allObjects as! [DataSnapshot] {
                        //getting values
                        let objectValue = objectData.value as? [String: AnyObject]
                        let objectTitle: String = objectValue?["title"] as! String
                        print("objectTitle --->>> ",objectTitle);
                        
                        let objectContent: String  = objectValue?["content"] as! String
                        let objectContentType: String  = objectValue?["type"] as! String
                        
                        let objectSubtitle: [String: AnyObject]  = objectValue?["subtitle"] as! [String : AnyObject]
                        
                        if (objectTitle == "anatomy")
                        {
                            localDataClavicle.KneeAnatomy = objectContent
                            //                        localDataClavicle.clavicleAnatomyType = objectContentType
                            
                        }
                        else if (objectTitle == "fracture")
                        {
                            localDataClavicle.KneeFracture = objectContent
                            //                        localDataClavicle.clavicleFractureType = objectContentType
                        }
                        else if (objectTitle == "treatment")
                        {
                            localDataClavicle.KneeTreatment = objectContent
                            //                        localDataClavicle.clavicleNonOperativeTreatment = objectSubtitle["Non-operative Treatment"] as! String
                            //                        localDataClavicle.clavicleOperativeTreatment = objectSubtitle["Operative Treatment"] as! String
                        }
                        else if (objectTitle == "recovery")
                        {
                            localDataClavicle.KneeRecovery = objectContent
                            /*
                             localDataClavicle.clavicleRecovery = objectSubtitle["Non-operative Recovery"] as! String
                             localDataClavicle.clavicleRecovery = objectSubtitle["Operative Recovery"] as! String
                             */
                        }
                    }
                }
            })
            //---------------------------------------------------------------------
        }
        else if tabType == "Elbow" {
            
            //------------ ForearmNew --------------------------------------------
            
            let refForearm = Database.database().reference().child("ELBOWNEW")
            refForearm.observe(DataEventType.value, with: { (snapshot) in
                if snapshot.childrenCount > 0 {
                    for objectData in snapshot.children.allObjects as! [DataSnapshot] {
                        //getting values
                        let objectValue = objectData.value as? [String: AnyObject]
                        let objectTitle: String = objectValue?["title"] as! String
                        print("objectTitle --->>> ",objectTitle);
                        
                        let objectContent: String  = objectValue?["content"] as! String
                        let objectContentType: String  = objectValue?["type"] as! String
                        
                        let objectSubtitle: [String: AnyObject]  = objectValue?["subtitle"] as! [String : AnyObject]
                        
                        if (objectTitle == "anatomy")
                        {
                            localDataClavicle.foreArmAnatomy = objectContent
                            //                        localDataClavicle.clavicleAnatomyType = objectContentType
                            
                        }
                        else if (objectTitle == "fracture")
                        {
                            localDataClavicle.foreArmFracture = objectContent
                            //                        localDataClavicle.clavicleFractureType = objectContentType
                        }
                        else if (objectTitle == "treatment")
                        {
                            localDataClavicle.foreArmTreatment = objectContent
                            //                        localDataClavicle.clavicleNonOperativeTreatment = objectSubtitle["Non-operative Treatment"] as! String
                            //                        localDataClavicle.clavicleOperativeTreatment = objectSubtitle["Operative Treatment"] as! String
                        }
                        else if (objectTitle == "recovery")
                        {
                            localDataClavicle.foreArmRecovery = objectContent
                            /*
                             localDataClavicle.clavicleRecovery = objectSubtitle["Non-operative Recovery"] as! String
                             localDataClavicle.clavicleRecovery = objectSubtitle["Operative Recovery"] as! String
                             */
                        }
                    }
                }
            })
            
            //---------------------------------------------------------------------
        }
        //---------------------------------------------------------------------

    }
    
    @objc func didTapView(){
        textPopUpView.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tapRecognizer.addTarget(self, action: #selector(FirstDetailViewController.didTapView))
        
//        if tabType == "Clavicle"
//        {
//            if innerTabType == "Fracture"
//            {
//
//            }
//        }
//        self.view.addGestureRecognizer(tapRecognizer)
        
        lblBoneType.text = tabType
        
        tableViewList.register(UINib(nibName: CellIdentifiers.anatomyCell1, bundle: nil), forCellReuseIdentifier: CellIdentifiers.anatomyCell1)
        tableViewList.register(UINib(nibName: CellIdentifiers.anatomyCell2, bundle: nil), forCellReuseIdentifier: CellIdentifiers.anatomyCell2)
        tableViewList.register(UINib(nibName: CellIdentifiers.anatomyCell3, bundle: nil), forCellReuseIdentifier: CellIdentifiers.anatomyCell3)
        tableViewList.register(UINib(nibName: CellIdentifiers.anatomyCell4, bundle: nil), forCellReuseIdentifier: CellIdentifiers.anatomyCell4)

        tableViewList.register(UINib(nibName: CellIdentifiers.fractureCell1, bundle: nil), forCellReuseIdentifier: CellIdentifiers.fractureCell1)
        tableViewList.register(UINib(nibName: CellIdentifiers.fractureCell2, bundle: nil), forCellReuseIdentifier: CellIdentifiers.fractureCell2)
        tableViewList.register(UINib(nibName: CellIdentifiers.fractureCell3, bundle: nil), forCellReuseIdentifier: CellIdentifiers.fractureCell3)
        
        tableViewList.register(UINib(nibName: CellIdentifiers.recoveryCell1, bundle: nil), forCellReuseIdentifier: CellIdentifiers.recoveryCell1)

        tableViewList.register(UINib(nibName: CellIdentifiers.expandCellWithTextOnly, bundle: nil), forCellReuseIdentifier: CellIdentifiers.expandCellWithTextOnly)

        tableViewList.register(UINib(nibName: CellIdentifiers.expandCellWithTextImage, bundle: nil), forCellReuseIdentifier: CellIdentifiers.expandCellWithTextImage)

        
        tableViewList.estimatedRowHeight = 100
        tableViewList.rowHeight = UITableViewAutomaticDimension
        
        
        if tabType == "Clavicle" {
            innerTabType = "Anatomy"
            self.reloadContents()
        }
        else if tabType == "Proximal Humerus" {
            innerTabType = "Anatomy"
            self.reloadContents()
        }
        if tabType == "Leg" {
            innerTabType = "Anatomy"
            self.reloadContents()
        }
        else if tabType == "Wrist" {
            innerTabType = "Anatomy"
            self.reloadContents()
        }
        if tabType == "Ankle" {
            innerTabType = "Anatomy"
            self.reloadContents()
        }
        else if tabType == "Forearm" {
            innerTabType = "Anatomy"
            self.reloadContents()
        }
        if tabType == "Femur" {
            innerTabType = "Anatomy"
            self.reloadContents()
        }
        if tabType == "Knee" {
            innerTabType = "Anatomy"
            self.reloadContents()
        }
        else if tabType == "Elbow" {
            innerTabType = "Anatomy"
            self.reloadContents()
        }
        
        anatomyBtn.setTitleColor(Color.Green.Medium, for: .normal)
        anatomyBtn.backgroundColor = UIColor.white
        
        fractureBtn.setTitleColor(UIColor.white, for: .normal)
        fractureBtn.backgroundColor = Color.Green.Medium
        
        treatmentBtn.setTitleColor(UIColor.white, for: .normal)
        treatmentBtn.backgroundColor = Color.Green.Medium
        
        recoveryBtn.setTitleColor(UIColor.white, for: .normal)
        recoveryBtn.backgroundColor = Color.Green.Medium
        
        
        anatomyBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 19)
        fractureBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        treatmentBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        recoveryBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    @IBAction func backToHome(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "removeTabbarView"), object: nil)
    }
    
    @IBAction func anatomyTabAction(_ sender: Any) {
        
        self.view.removeGestureRecognizer(tapRecognizer)
        
        innerTabType = "Anatomy"
        addingCell = false
        self.scrollTableViewToRow(0)

        self.reloadContents()
        
        anatomyBtn.setTitleColor(Color.Green.Medium, for: .normal)
        anatomyBtn.backgroundColor = UIColor.white
        
        fractureBtn.setTitleColor(UIColor.white, for: .normal)
        fractureBtn.backgroundColor = Color.Green.Medium
        
        treatmentBtn.setTitleColor(UIColor.white, for: .normal)
        treatmentBtn.backgroundColor = Color.Green.Medium
        
        recoveryBtn.setTitleColor(UIColor.white, for: .normal)
        recoveryBtn.backgroundColor = Color.Green.Medium
        
        anatomyBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 19)
        fractureBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        treatmentBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        recoveryBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)

    }
    
    @IBAction func fractureTabAction(_ sender: Any) {
        
        self.view.addGestureRecognizer(tapRecognizer)

        
        innerTabType = "Fracture"
        addingCell = false
        self.scrollTableViewToRow(0)

        self.reloadContents()
        
        anatomyBtn.setTitleColor(UIColor.white, for: .normal)
        anatomyBtn.backgroundColor = Color.Green.Medium
        
        fractureBtn.setTitleColor(Color.Green.Medium, for: .normal)
        fractureBtn.backgroundColor = UIColor.white
        
        treatmentBtn.setTitleColor(UIColor.white, for: .normal)
        treatmentBtn.backgroundColor = Color.Green.Medium
        
        recoveryBtn.setTitleColor(UIColor.white, for: .normal)
        recoveryBtn.backgroundColor = Color.Green.Medium
        
        
        anatomyBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        fractureBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 19)
        treatmentBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        recoveryBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
    }
    
    @IBAction func treatmentTabAction(_ sender: Any) {
        
        self.view.removeGestureRecognizer(tapRecognizer)

        innerTabType = "Treatment"
        addingCell = false
        self.scrollTableViewToRow(0)

        self.reloadContents()
        
        anatomyBtn.setTitleColor(UIColor.white, for: .normal)
        anatomyBtn.backgroundColor = Color.Green.Medium
        
        fractureBtn.setTitleColor(UIColor.white, for: .normal)
        fractureBtn.backgroundColor = Color.Green.Medium
        
        treatmentBtn.setTitleColor(Color.Green.Medium, for: .normal)
        treatmentBtn.backgroundColor = UIColor.white
        
        recoveryBtn.setTitleColor(UIColor.white, for: .normal)
        recoveryBtn.backgroundColor = Color.Green.Medium
        
        
        anatomyBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        fractureBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        treatmentBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 19)
        recoveryBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
    }
    
    @IBAction func recoveryTabAction(_ sender: Any) {
        
        self.view.removeGestureRecognizer(tapRecognizer)

        innerTabType = "Recovery"
        addingCell = false
        self.scrollTableViewToRow(0)

        self.reloadContents()
        
        anatomyBtn.setTitleColor(UIColor.white, for: .normal)
        anatomyBtn.backgroundColor = Color.Green.Medium
        
        fractureBtn.setTitleColor(UIColor.white, for: .normal)
        fractureBtn.backgroundColor = Color.Green.Medium
        
        treatmentBtn.setTitleColor(UIColor.white, for: .normal)
        treatmentBtn.backgroundColor = Color.Green.Medium
        
        recoveryBtn.setTitleColor(Color.Green.Medium, for: .normal)
        recoveryBtn.backgroundColor = UIColor.white
        
        anatomyBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        fractureBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        treatmentBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        recoveryBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 19)
    }
    
    func scrollTableViewToRow(_ row:Int) {
            let seconds = 0.1
            let delay = seconds * Double(NSEC_PER_SEC)  // nanoseconds per seconds
            let dispatchTime = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
            
            DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
                self.tableViewList.scrollToRow(at: IndexPath(row: row, section: 0), at: UITableViewScrollPosition.top, animated: true)
            })
        
    }
    
    func reloadContents() {
        tableViewList.delegate = self
        tableViewList.dataSource = self
        tableViewList.reloadData()
    }
    
    /*
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dictionaryOfArrayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var celll:UITableViewCell?
        
        let cellContent = dictionaryOfArrayData[indexPath.row]
            
        if let content = cellContent["summary"] as? String
        {
            if content != ""{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.clavicleAnatomy)
                return cell
            }
            
        }
        if let content = cellContent["presscription"] as? String
        {
            if content != ""{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell3, for: indexPath) as! AnatomyTableViewCell3
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.clavicleAnatomyType)
                return cell
            }
            
        }
        if let content = cellContent["header"] as? String
        {
            if content != ""{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell(content)
                return cell
            }
            
        }
        if let content = cellContent["detail"] as? String
        {
            if content != ""{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.expandCellWithTextOnly, for: indexPath) as! ExpandTableViewCellWithText
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.clavicleRecovery)
                return cell
            }
            
        }
        if let content = cellContent["image"] as? String
        {
            if content != ""{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell4, for: indexPath) as! AnatomyTableViewCell4
                cell.selectionStyle = .none
                let imageName = "clavicle_anatomy.png"
                let image = UIImage(named: imageName)
                cell.updateCell(image!)
                return cell
            }
        }
        
        return celll!
    }
    */
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        if tabType == "Clavicle" {
            
            if innerTabType == "Anatomy"
            {
                 return clavicleAnatomyArrayCount
            }
            if innerTabType == "Fracture"
            {
                return clavicleFractureArrayCount
            }
            if innerTabType == "Treatment"
            {
                return clavicleTreatmentArrayCount
            }
            if innerTabType == "Recovery"
            {
                return clavicleRecoveryArrayCount
            }
        }
        else if tabType == "Proximal Humerus" {
            
            if innerTabType == "Anatomy"
            {
                return proximalAnatomyArrayCount
            }
            if innerTabType == "Fracture"
            {
                return proximalFractureArrayCount
            }
            if innerTabType == "Treatment"
            {
                return proximalTreatmentArrayCount
            }
            if innerTabType == "Recovery"
            {
                return proximalRecoveryArrayCount
            }
        }
        else{
            return 1
        }
        return 1
    }
    
    
    
    
    @objc func pressed(sender: UIButton!) {
        
        textPopUpView.isHidden = false

        /*
        var alertView = UIAlertView()
        alertView.addButton(withTitle: "Ok")
        alertView.title = "title"
        alertView.message = "message"
        alertView.show()
        */
    }

    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var celll:UITableViewCell?
        
        if tabType == "Clavicle" {
            
            if innerTabType == "Anatomy"
            {
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                    cell.selectionStyle = .none
                    cell.labelSummary.textAlignment = .left
                    cell.updateCell(localDataClavicle.clavicleAnatomy)
                    return cell
                }
                else if indexPath.row == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell4, for: indexPath) as! AnatomyTableViewCell4
                    cell.selectionStyle = .none
                    let imageName = "clavicle_anatomy.png"
                    let image = UIImage(named: imageName)
                    cell.updateCell(image!)
                    return cell
                }
                else if indexPath.row == 2 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell3, for: indexPath) as! AnatomyTableViewCell3
                    cell.selectionStyle = .none
                    cell.labelSummary.textAlignment = .left
                    cell.updateCell(localDataClavicle.clavicleAnatomyType)
                    return cell
                }
                /*
                else if indexPath.row == 3  {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                    cell.selectionStyle = .none
                    cell.updateCell("Operative1")
                    if addingCell && selectedIndex == indexPath.row {
                        cell.data.isHidden = false
                        cell.data.text = localDataClavicle.clavicleRecovery
                        cell.contentView.setNeedsLayout()
                    } else {
                        cell.data.isHidden = true
                        cell.data.text = ""
                        cell.contentView.setNeedsLayout()
                    }
                    
                    return cell
                }
                else if indexPath.row == 4 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                    cell.selectionStyle = .none
                    cell.updateCell("Operative2")
                    if addingCell && selectedIndex == indexPath.row {
                        cell.data.isHidden = false
                        cell.data.text = localDataClavicle.clavicleRecovery
                        cell.contentView.setNeedsLayout()
                    } else {
                        cell.data.isHidden = true
                        cell.data.text = ""
                        cell.contentView.setNeedsLayout()
                    }
                    return cell
                }
                else if indexPath.row == 5   {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                    cell.selectionStyle = .none
                    cell.updateCell("Operative3")
                    if addingCell && selectedIndex == indexPath.row {
                        cell.data.isHidden = false
                        cell.data.text = localDataClavicle.clavicleRecovery
                        cell.contentView.setNeedsLayout()
                    } else {
                        cell.data.isHidden = true
                        cell.data.text = ""
                        cell.contentView.setNeedsLayout()
                    }
                    return cell
                }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.expandCellWithTextOnly, for: indexPath) as! ExpandTableViewCellWithText
                    cell.selectionStyle = .none
                    cell.updateCell(localDataClavicle.clavicleRecovery)
                    return cell
                }
                */
            }
            else if innerTabType == "Fracture"
            {
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.fractureCell1, for: indexPath) as! FractureTableViewCell1
                    cell.selectionStyle = .none
                    cell.labelSummary.textAlignment = .left
                    cell.updateCell(localDataClavicle.clavicleFracture)
                
                    let myFirstButton:UIButton = UIButton(frame: CGRect(x: 50, y: 20, width: 140, height: 50))
                    myFirstButton.backgroundColor = UIColor.clear
                    myFirstButton.tintColor = UIColor.clear
                    myFirstButton.addTarget(self, action:#selector(self.pressed(sender:)), for: .touchUpInside)

                    cell.labelSummary.addSubview(myFirstButton)
                    
                    
                    textPopUpView.frame = CGRect(x: 20.0, y: 60.0, width: 300.0, height: 200.0)
                    textPopUpView.text = "A growth plate is an area of cartilage that regulates the length and shape of bone. Until puberty, a growth plate is 'open' which means there is a active lengthening of bone. After puberty, growth plates have generally 'closed' and bones are considered mature."
                    textPopUpView.textAlignment = NSTextAlignment.center
                    textPopUpView.textColor = UIColor.black
                    textPopUpView.backgroundColor = UIColor.white
                    textPopUpView.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 15)
                    textPopUpView.isHidden = true
                    textPopUpView.isEditable = false
                    textPopUpView.isUserInteractionEnabled = false
                    tableViewList.addSubview(textPopUpView)
                    textPopUpView.bringSubview(toFront: tableViewList)
                    
                    //-------------------------------------------------
                    /*
                    let attributes = [NSForegroundColorAttributeName: UIColor.black,
                                      NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
                    */
                    
                    cell.labelSummary.attributedText = NSAttributedString(string: localDataClavicle.clavicleFracture, attributes: nil)
                    cell.labelSummary.isUserInteractionEnabled = true
                    
                    //Step 2: Define a selection handler block
                    let handler = {
                        (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
                        let controller = UIAlertController(title: substring, message: nil, preferredStyle: UIAlertControllerStyle.alert)
                        controller.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                        print("Touched")
                        self.present(controller, animated: true, completion: nil)
                    }
                    
                    //Step 3: Add link substrings
                    cell.labelSummary.setLinksForSubstrings(["growth plates"], withLinkHandler: handler)
                    //-------------------------------------------------
 
                    
                    return cell
                }
                else if indexPath.row == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell4, for: indexPath) as! AnatomyTableViewCell4
                    cell.selectionStyle = .none
                    let imageName = "clavicle_anatomy.png"
                    let image = UIImage(named: imageName)
                    cell.updateCell(image!)
                    return cell
                }
                else if indexPath.row == 2 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.fractureCell3, for: indexPath) as! FractureTableViewCell3
                    cell.selectionStyle = .none
                    cell.labelSummary.textAlignment = .left
                    cell.updateCell(localDataClavicle.clavicleFractureType)
                    return cell
                }
            }
            else if innerTabType == "Treatment"
            {
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                    cell.selectionStyle = .none
                    cell.labelSummary.textAlignment = .left
                    cell.updateCell(localDataClavicle.clavicleTreatment)
                    return cell
                }
                else if indexPath.row == 1  {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                    cell.selectionStyle = .none
                    cell.updateCell("Non-operative Treatment", color1: Color.Green.Medium, color2: Color.Green.Light)
                    if addingCell && selectedIndex == indexPath.row {
                        cell.data.isHidden = false
                        cell.data.text = localDataClavicle.clavicleNonOperativeTreatment
                        cell.contentView.setNeedsLayout()
                    } else {
                        cell.data.isHidden = true
                        cell.data.text = ""
                        cell.contentView.setNeedsLayout()
                    }
                    
                    return cell
                }
                else if indexPath.row == 2 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                    cell.selectionStyle = .none
                    cell.updateCell("Operative Treatment", color1: Color.Green.Medium, color2: Color.Green.Light)
                    if addingCell && selectedIndex == indexPath.row {
                        cell.data.isHidden = false
                        cell.data.text = localDataClavicle.clavicleOperativeTreatment
                        cell.contentView.setNeedsLayout()
                    } else {
                        cell.data.isHidden = true
                        cell.data.text = ""
                        cell.contentView.setNeedsLayout()
                    }
                    return cell
                }
            }
            else if innerTabType == "Recovery"
            {
                if indexPath.row == 0  {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                    cell.selectionStyle = .none
                    cell.updateCell("Non-operative Recovery", color1: Color.Green.Medium, color2: Color.Green.Light)
                    if addingCell && selectedIndex == indexPath.row {
                        cell.data.isHidden = false
                        cell.data.text = localDataClavicle.clavicleNonOperativeRecovery
                        cell.contentView.setNeedsLayout()
                    } else {
                        cell.data.isHidden = true
                        cell.data.text = ""
                        cell.contentView.setNeedsLayout()
                    }
                    
                    return cell
                }
                else if indexPath.row == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                    cell.selectionStyle = .none
                    cell.updateCell("Operative Recovery", color1: Color.Green.Medium, color2: Color.Green.Light)
                    if addingCell && selectedIndex == indexPath.row {
                        cell.data.isHidden = false
                        cell.data.text = localDataClavicle.clavicleOperativeRecovery
                        cell.contentView.setNeedsLayout()
                    } else {
                        cell.data.isHidden = true
                        cell.data.text = ""
                        cell.contentView.setNeedsLayout()
                    }
                    return cell
                }
                /*
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.recoveryCell1, for: indexPath) as! RecoveryTableViewCell1
                    cell.selectionStyle = .none
                    cell.updateCell(localDataClavicle.clavicleRecovery)
                    return cell
                }
                */
            }
        }
        if tabType == "Proximal Humerus" {
            
            if innerTabType == "Anatomy"
            {
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                    cell.selectionStyle = .none
                    cell.updateCell(localDataClavicle.proximalAnatomy)
                    return cell
                }
                else if indexPath.row == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell4, for: indexPath) as! AnatomyTableViewCell4
                    cell.selectionStyle = .none
                    let imageName = "clavicle_anatomy.png"
                    let image = UIImage(named: imageName)
                    cell.updateCell(image!)
                    return cell
                }
                else if indexPath.row == 2 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell3, for: indexPath) as! AnatomyTableViewCell3
                    cell.selectionStyle = .none
                    cell.updateCell(localDataClavicle.proximalAnatomyType)
                    return cell
                }
            }
            else if innerTabType == "Fracture"
            {
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.fractureCell1, for: indexPath) as! FractureTableViewCell1
                    cell.selectionStyle = .none
                    cell.updateCell(localDataClavicle.proximalFracture)
                    return cell
                }
                else if indexPath.row == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.fractureCell1, for: indexPath) as! FractureTableViewCell1
                    cell.selectionStyle = .none
                    cell.updateCell(localDataClavicle.proximalFractureType1)
                    return cell
                }
                else if indexPath.row == 2 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.fractureCell3, for: indexPath) as! FractureTableViewCell3
                    cell.selectionStyle = .none
                    cell.updateCell(localDataClavicle.proximalFractureType2)
                    return cell
                }
                
            }
            else if innerTabType == "Treatment"
            {
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                    cell.selectionStyle = .none
                    cell.labelSummary.textAlignment = .left
                    cell.updateCell(localDataClavicle.proximalTreatment)
                    return cell
                }
                if indexPath.row == 1  {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                    cell.selectionStyle = .none
                    cell.updateCell("Non-operative Treatment", color1: Color.Green.Medium, color2: Color.Green.Light)
                    if addingCell && selectedIndex == indexPath.row {
                        cell.data.isHidden = false
                        cell.data.text = localDataClavicle.clavicleRecovery
                        cell.contentView.setNeedsLayout()
                    } else {
                        cell.data.isHidden = true
                        cell.data.text = ""
                        cell.contentView.setNeedsLayout()
                    }
                    
                    return cell
                }
                else if indexPath.row == 2 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                    cell.selectionStyle = .none
                    cell.updateCell("Operative Treatment", color1: Color.Green.Medium, color2: Color.Green.Light)
                    if addingCell && selectedIndex == indexPath.row {
                        cell.data.isHidden = false
                        cell.data.text = localDataClavicle.clavicleRecovery
                        cell.contentView.setNeedsLayout()
                    } else {
                        cell.data.isHidden = true
                        cell.data.text = ""
                        cell.contentView.setNeedsLayout()
                    }
                    return cell
                }
            }
            else if innerTabType == "Recovery"
            {
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.recoveryCell1, for: indexPath) as! RecoveryTableViewCell1
                    cell.selectionStyle = .none
                    cell.updateCell(localDataClavicle.proximalRecovery)
                    return cell
                }
            }
        }
        else if tabType == "Leg" {
            
            if innerTabType == "Anatomy"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.LegAnatomy)
                return cell
            }
            else if innerTabType == "Fracture"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.LegFracture)
                return cell
            }
            else if innerTabType == "Treatment"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.LegTreatment)
                return cell
            }
            else if innerTabType == "Recovery"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.LegRecovery)
                return cell
            }
            
        }
        else if tabType == "Femur" {
            
            if innerTabType == "Anatomy"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.FemurAnatomy)
                return cell
            }
            else if innerTabType == "Fracture"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.FemurFracture)
                return cell
            }
            else if innerTabType == "Treatment"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.FemurTreatment)
                return cell
            }
            else if innerTabType == "Recovery"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.FemurRecovery)
                return cell
            }
            
        }
        else if tabType == "Wrist" {
            
            if innerTabType == "Anatomy"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.labelSummary.textAlignment = .left

                cell.updateCell(localDataClavicle.WristAnatomy)
                return cell
            }
            else if innerTabType == "Fracture"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.labelSummary.textAlignment = .left

                cell.updateCell(localDataClavicle.WristFracture)
                return cell
            }
            else if innerTabType == "Treatment"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.labelSummary.textAlignment = .left

                cell.updateCell(localDataClavicle.WristTreatment)
                return cell
            }
            else if innerTabType == "Recovery"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.labelSummary.textAlignment = .left

                cell.updateCell(localDataClavicle.WristRecovery)
                return cell
            }
            
        }
        else if tabType == "Forearm" {
            
            if innerTabType == "Anatomy"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.foreArmAnatomy)
                return cell
            }
            else if innerTabType == "Fracture"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.foreArmFracture)
                return cell
            }
            else if innerTabType == "Treatment"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.foreArmTreatment)
                return cell
            }
            else if innerTabType == "Recovery"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.foreArmRecovery)
                return cell
            }
            
        }
        else if tabType == "Ankle" {
            
            if innerTabType == "Anatomy"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.AnkleAnatomy)
                return cell
            }
            else if innerTabType == "Fracture"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.AnkleFracture)
                return cell
            }
            else if innerTabType == "Treatment"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.AnkleTreatment)
                return cell
            }
            else if innerTabType == "Recovery"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.AnkleRecovery)
                return cell
            }
            
        }
        else if tabType == "Knee" {
            
            if innerTabType == "Anatomy"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.KneeAnatomy)
                return cell
            }
            else if innerTabType == "Fracture"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.KneeFracture)
                return cell
            }
            else if innerTabType == "Treatment"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.KneeTreatment)
                return cell
            }
            else if innerTabType == "Recovery"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.KneeRecovery)
                return cell
            }
            
        }
        else if tabType == "Elbow" {
            
            if innerTabType == "Anatomy"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.labelSummary.textAlignment = .left
                cell.updateCell(localDataClavicle.elbowAnatomy)
                return cell
            }
            else if innerTabType == "Fracture"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.labelSummary.textAlignment = .left
                cell.updateCell(localDataClavicle.elbowfracture)
                return cell
            }
            else if innerTabType == "Treatment"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.labelSummary.textAlignment = .left
                cell.updateCell(localDataClavicle.elbowtreatment)
                return cell
            }
            else if innerTabType == "Recovery"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.labelSummary.textAlignment = .left
                cell.updateCell(localDataClavicle.elbowRecovery)
                return cell
            }
            
        }
        
        return celll!
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UIDevice.current.userInterfaceIdiom != .pad {
            
            if tabType == "Clavicle" {
                
                if innerTabType == "Anatomy"
                {
                    if indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5
                    {
                        if addingCell == false
                        {
                            addingCell = true
//                            self.addDetailCellIntoView(indexPath)
                        }
                        else{
                            addingCell = false
//                            self.removeDetailCellIntoView(indexPath)
                        }
                        tableView.reloadData()
                        selectedIndex = indexPath.row
                        scrollTableViewToRow(indexPath.row)
                    }
                }
                else if innerTabType == "Treatment"
                {
                    if indexPath.row == 1 || indexPath.row == 2
                    {
                        if addingCell == false
                        {
                            addingCell = true
//                            self.addDetailCellIntoView(indexPath)
                        }
                        else{
                            addingCell = false
//                            self.removeDetailCellIntoView(indexPath)
                        }
                        tableView.reloadData()
                        selectedIndex = indexPath.row
                        scrollTableViewToRow(indexPath.row)
                    }
                }
                else if innerTabType == "Recovery"
                {
                    if indexPath.row == 0 || indexPath.row == 1
                    {
                        if addingCell == false
                        {
                            addingCell = true
//                            self.addDetailCellIntoView(indexPath)
                        }
                        else{
                            addingCell = false
//                            self.removeDetailCellIntoView(indexPath)
                        }
                        tableView.reloadData()
                        selectedIndex = indexPath.row
                        scrollTableViewToRow(indexPath.row)
                    }
                }
                
            }
            else if tabType == "Proximal Humerus" {
                
                if innerTabType == "Treatment"
                {
                    if indexPath.row == 1 || indexPath.row == 2
                    {
                        if addingCell == false
                        {
                            addingCell = true
//                            self.addDetailCellIntoView(indexPath)
                        }
                        else{
                            addingCell = false
//                            self.removeDetailCellIntoView(indexPath)
                        }
                        tableView.reloadData()
                        selectedIndex = indexPath.row
                        scrollTableViewToRow(indexPath.row)
                    }
                }
            }
            
        }
    }

    func showDetailsCell(){
    }
    
    func addDetailCellIntoView(_ currentIndexPath:IndexPath){
        
        clavicleAnatomyArrayCount = clavicleAnatomyArrayCount + 1
        
        var paths: [IndexPath] = []
        for i in 1...1 {
            let indexPath = IndexPath(row: currentIndexPath.row+i, section: currentIndexPath.section)
            paths.append(indexPath)
        }
        
        dictionaryOfArrayData.insert(arrayData, at: currentIndexPath.row+1)
        
        tableViewList.beginUpdates()
        tableViewList.insertRows(at: paths, with: UITableViewRowAnimation.automatic)
        tableViewList.endUpdates()
    }
    
    func removeDetailCellIntoView(_ currentIndexPath:IndexPath){
        
        clavicleAnatomyArrayCount = clavicleAnatomyArrayCount - 1
        
        var paths: [IndexPath] = []
        for i in 1...1 {
            let indexPath = IndexPath(row: currentIndexPath.row+i, section: currentIndexPath.section)
            paths.append(indexPath)
        }
        
        dictionaryOfArrayData.remove(at: currentIndexPath.row+1)

        tableViewList.beginUpdates()
        tableViewList.deleteRows(at: paths, with: UITableViewRowAnimation.automatic)
        tableViewList.endUpdates()
    }
    
    @IBAction func menuAction(_ sender: Any) {
        
        if isSelect == false
        {
            isSelect = true
            menuOptionView.isHidden = false
        }
        else{
            isSelect = false
            menuOptionView.isHidden = true
        }
    }
    
    @IBAction func faqAction(_ sender: UIButton) {
        
        isSelect = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FaqViewController") as? FaqViewController
        self.present(vc!, animated: false) {
            
        }
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        viewFaq.layer.add(transition, forKey: "animation")
        self.viewFaq.isHidden = false
        */
    }
    
    @IBAction func notesAction(_ sender: Any) {
        
        isSelect = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NotesViewController") as? NotesViewController
        self.present(vc!, animated: false) {
            
        }
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        notesView.layer.add(transition, forKey: "animation")
        self.notesView.isHidden = false
        
        notesTextView.text = UserDefaults.standard.object(forKey: "notes") as? String
        */
    }
}
