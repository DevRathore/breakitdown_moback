//
//  FirstViewController.swift
//  BreakItDown
//
//  Created by Dobango on 05/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    @IBOutlet weak var tableViewList: UITableView!
    @IBOutlet weak var btnElbow: UIButton!
    @IBOutlet weak var notesView: UIView!
    @IBOutlet weak var menuOptionView: UIView!
    @IBOutlet weak var viewFaq: UIView!
    @IBOutlet weak var notesTextView: UITextView!
    
    var isSelect: Bool = false
    
    var arrayWithOption = ["Supracondylar humerus","Lateral condyle","Medial epicondyle","Proximal radius"]

    override func viewWillDisappear(_ animated: Bool) {
        tableViewList.isHidden = true
        btnElbow.isSelected = false
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

//        let navBackgroundImage:UIImage! = UIImage(named: "icn_logo_large_01_")
//        self.navigationController?.navigationBar.setBackgroundImage(navBackgroundImage,for: .default)
        
        tableViewList.register(UINib(nibName: CellIdentifiers.selectionCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.selectionCell)
        
        tableViewList.estimatedRowHeight = 100
        tableViewList.rowHeight = UITableViewAutomaticDimension
        
        tableViewList.delegate = self
        tableViewList.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    
    @IBAction func faqAction(_ sender: UIButton) {
        
        isSelect = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FaqViewController") as? FaqViewController
        self.present(vc!, animated: false) {
            
        }
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        viewFaq.layer.add(transition, forKey: "animation")
        self.viewFaq.isHidden = false
        */

    }
    
    @IBAction func notesAction(_ sender: Any) {
        
        isSelect = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NotesViewController") as? NotesViewController
        self.present(vc!, animated: false) {
            
        }
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        notesView.layer.add(transition, forKey: "animation")
        self.notesView.isHidden = false
         notesTextView.text = UserDefaults.standard.object(forKey: "notes") as? String

        */
    }
    
    /*
    @IBAction func backFaqAction(_ sender: UIButton) {
        
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeRemoved
        transition.duration = 0.5
        transition.subtype = kCATransitionFromBottom
        viewFaq.layer.add(transition, forKey: "animation")
        viewFaq.isHidden = true
    }
    */
    
   
    
    @IBAction func backAction(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "removeTabbarView"), object: nil)
    }
    
    @IBAction func backToHomeAction(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "removeTabbarView"), object: nil)
    }
    
    @IBAction func menuAction(_ sender: Any) {
        
        if isSelect == false
        {
            isSelect = true
            menuOptionView.isHidden = false
            
        }
        else{
            isSelect = false
            menuOptionView.isHidden = true
        }
 
    }
    
    
    
    @IBAction func proximalAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FirstDetailViewController") as? FirstDetailViewController
        vc?.tabType = "Proximal Humerus"
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func clavicleAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FirstDetailViewController") as? FirstDetailViewController
        vc?.tabType = "Clavicle"
        self.navigationController?.pushViewController(vc!, animated: true)

    }
    
    @IBAction func elbowAction(_ sender: UIButton) {
        
        if sender.isSelected == false {
            tableViewList.isHidden = false
            sender.isSelected = true
        }
        else{
            tableViewList.isHidden = true
            sender.isSelected = false
            
            /*
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FirstDetailViewController") as? FirstDetailViewController
            vc?.tabType = "Elbow"
            self.navigationController?.pushViewController(vc!, animated: true)
            */
        }
        

    }
    
    @IBAction func forearmAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FirstDetailViewController") as? FirstDetailViewController
        vc?.tabType = "Forearm"
        self.navigationController?.pushViewController(vc!, animated: true)

    }
    
    @IBAction func wristAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FirstDetailViewController") as? FirstDetailViewController
        vc?.tabType = "Wrist"
        self.navigationController?.pushViewController(vc!, animated: true)

    }
    
    @IBAction func kneeAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FirstDetailViewController") as? FirstDetailViewController
        vc?.tabType = "Knee"
        self.navigationController?.pushViewController(vc!, animated: true)

    }
    
    @IBAction func femurAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FirstDetailViewController") as? FirstDetailViewController
        vc?.tabType = "Femur"
        self.navigationController?.pushViewController(vc!, animated: true)

    }
    
    @IBAction func legAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FirstDetailViewController") as? FirstDetailViewController
        vc?.tabType = "Leg"
        self.navigationController?.pushViewController(vc!, animated: true)

    }
    
    @IBAction func ankleAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FirstDetailViewController") as? FirstDetailViewController
        vc?.tabType = "Ankle"
        self.navigationController?.pushViewController(vc!, animated: true)

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        var celll:UITableViewCell?
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.selectionCell, for: indexPath) as! SelectionTableViewCell
        cell.selectionStyle = .none
        cell.updateCell(arrayWithOption[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UIDevice.current.userInterfaceIdiom != .pad {
            
            tableViewList.isHidden = true
            btnElbow.isSelected = false
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FirstDetailViewController") as? FirstDetailViewController
            vc?.tabType = "Elbow"
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
}
