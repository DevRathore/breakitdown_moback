//
//  ForthViewController.swift
//  BreakItDown
//
//  Created by Dobango on 05/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit
import EventKit
import UserNotifications


class ForthViewController: UIViewController, CalendarViewDataSource, CalendarViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var hourTxtFld: UITextField!
    
    @IBOutlet weak var minuteTxtFld: UITextField!
    
    @IBOutlet weak var saveBtn: UIButton!
    
    @IBOutlet weak var deadlinePicker: UIDatePicker!
    
    @IBOutlet weak var tableViewList: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var calendarView: CalendarView!
    @IBOutlet weak var lblOption: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    
    @IBOutlet weak var btnActionData: UIButton!
    @IBOutlet weak var btnCalendar: UIButton!
    
    @IBOutlet weak var menuOptionView: UIView!
    
    var isSelect: Bool = false
    
    var arrayWithOption = ["Clinic Visit","Cast Removal","Wound Care","Surgery"]
    let eventStore = EKEventStore()
    
    var hourSelected: Int = 0
    var minuteSelected: Int = 0
    
    var yearSelected: Int = 0
    var monthSelected: Int = 0
    var daySelected: Int = 0

    
    var currentHourSelected: Int = 0
    var currentMinuteSelected: Int = 0

    var currentYearSelected: Int = 0
    var currentMonthSelected: Int = 0
    var currentDaySelected: Int = 0

    let tapRecognizer = UITapGestureRecognizer()
    var savedDate = Date()
    
    var dateS: String = ""
    
    @objc func didTapView(){
        
//        hourTxtFld.resignFirstResponder()
//        minuteTxtFld.resignFirstResponder()
        
        self.view.endEditing(true)
        self.view.removeGestureRecognizer(tapRecognizer)

        //        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
    }
    
    func getTodayString() -> String{
        
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        
        let year = components.year
        let month = components.month
        let day = components.day
        
        currentHourSelected = components.hour!
        currentMinuteSelected = components.minute!
        currentYearSelected = components.year!
        currentMonthSelected = components.month!
        currentDaySelected = components.day!
        
        let hour = components.hour
        let minute = components.minute
        let second = components.second
        
        let today_string = String(year!) + "-" + String(month!) + "-" + String(day!) + " " + String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
        
        return today_string
        
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        
        let today : String!
        today = getTodayString()
        
        let center = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .sound];
        
        // Swift
        center.requestAuthorization(options: options) {
            (granted, error) in
            if !granted {
                print("Something went wrong")
            }
        }
        
        // Swift
        center.getNotificationSettings { (settings) in
            if settings.authorizationStatus != .authorized {
                // Notifications not allowed
            }
        }
        
        // Swift
        let content = UNMutableNotificationContent()
        content.title = "Don't forget"
        content.body = lblOption.text!
        content.sound = UNNotificationSound.default()
        
        
        let gregorian = Calendar(identifier: .gregorian)
        
        var components2 = gregorian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: savedDate)
        
        components2.hour = hourSelected
        components2.minute = minuteSelected
        components2.second = 0
        
        let year = components2.year
        let month = components2.month
        let day = components2.day
        
        /*
         components2.hour = 10
         components2.minute = 30
         components2.second = 0
         */
        
        let date2 = gregorian.date(from: components2)!
        
        
        let triggerDaily = Calendar.current.dateComponents([.hour,.minute,.second,], from: date2)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
        
        
        // Swift
        let identifier = "UYLLocalNotification"
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: trigger)
        
        center.add(request, withCompletionHandler: { (error) in
            if let error = error {
                // Something went wrong
            }
        })
        
        
        // Swift
        let snoozeAction = UNNotificationAction(identifier: "Snooze",
                                                title: "Snooze", options: [])
        let deleteAction = UNNotificationAction(identifier: "UYLDeleteAction",
                                                title: "Delete", options: [.destructive])
        
        // Swift
        let category = UNNotificationCategory(identifier: "UYLReminderCategory",
                                              actions: [snoozeAction,deleteAction],
                                              intentIdentifiers: [], options: [])
        
        // Swift
        center.setNotificationCategories([category])
        
        content.categoryIdentifier = "UYLReminderCategory"
        
        if currentDaySelected == day {
            if currentHourSelected <= hourSelected && currentMinuteSelected <= minuteSelected{
                
                if hourSelected < 10{
                    if minuteSelected < 10
                    {
                        lblDate.text = dateS + " at " + "0" + "\(hourSelected)" + ":" + "0" + "\(minuteSelected)"
                    }
                    else{
                        lblDate.text = dateS + " at " + "0" + "\(hourSelected)" + ":" + "\(minuteSelected)"
                    }
                }
                else if minuteSelected < 10
                {
                    lblDate.text = dateS + " at " + "\(hourSelected)" + ":" + "0" + "\(minuteSelected)"
                }
                else {
                    lblDate.text = dateS + " at " + "\(hourSelected)" + ":" + "\(minuteSelected)"
                    
                }
                
                
                UserDefaults.standard.set(lblDate.text, forKey: "date")
                
                UserDefaults.standard.set(hourTxtFld.text, forKey: "hour")
                UserDefaults.standard.set(minuteTxtFld.text, forKey: "minute")
            }
            else
            {
                let alert = UIAlertController(title: "Alert", message: "Please select future time", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        else {
            
            if hourSelected < 10{
                if minuteSelected < 10
                {
                    lblDate.text = dateS + " at " + "0" + "\(hourSelected)" + ":" + "0" + "\(minuteSelected)"
                }
                else{
                    lblDate.text = dateS + " at " + "0" + "\(hourSelected)" + ":" + "\(minuteSelected)"
                }
            }
            else if minuteSelected < 10
            {
                lblDate.text = dateS + " at " + "\(hourSelected)" + ":" + "0" + "\(minuteSelected)"
            }
            else {
                lblDate.text = dateS + " at " + "\(hourSelected)" + ":" + "\(minuteSelected)"
                
            }
            
            
            UserDefaults.standard.set(lblDate.text, forKey: "date")
            
            UserDefaults.standard.set(hourTxtFld.text, forKey: "hour")
            UserDefaults.standard.set(minuteTxtFld.text, forKey: "minute")
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

//        let tapRecognizer = UITapGestureRecognizer()
        tapRecognizer.addTarget(self, action: #selector(ForthViewController.didTapView))
        
        tableViewList.register(UINib(nibName: CellIdentifiers.selectionCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.selectionCell)
        
        tableViewList.estimatedRowHeight = 100
        tableViewList.rowHeight = UITableViewAutomaticDimension
        
        tableViewList.delegate = self
        tableViewList.dataSource = self
        
        CalendarView.Style.cellShape                = .round
        CalendarView.Style.cellColorDefault         = UIColor.clear
        CalendarView.Style.cellColorToday           = UIColor.clear
        CalendarView.Style.cellSelectedBorderColor  = UIColor.red
        CalendarView.Style.cellSelectedColor        = UIColor.red
        
        CalendarView.Style.cellEventColor           = UIColor.clear
        CalendarView.Style.headerTextColor          = UIColor.black
        
        CalendarView.Style.headerMonthTextColor     = UIColor.red
        CalendarView.Style.headerDayTextColor       = UIColor.black
        
        CalendarView.Style.cellTextColorDefault     = UIColor.black
        CalendarView.Style.cellTextColorToday       = UIColor.black
        
        CalendarView.Style.cellSelectedTextColor    = UIColor.white
        
        CalendarView.Style.firstWeekday             = .monday
        
        calendarView.dataSource = self
        calendarView.delegate = self
        
        calendarView.direction = .horizontal
        calendarView.multipleSelectionEnable = false
        calendarView.marksWeekends = false
        
        calendarView.backgroundColor = UIColor.white
        
//        self.SOGetPermissionCalendarAccess()
        
        if ((UserDefaults.standard.object(forKey: "date") as? String) != nil) {
            lblDate.text = UserDefaults.standard.object(forKey: "date") as? String
        }
        
        if ((UserDefaults.standard.object(forKey: "appointmentSubject") as? String) != nil) {
            lblOption.text = UserDefaults.standard.object(forKey: "appointmentSubject") as? String
        }
        
        if ((UserDefaults.standard.object(forKey: "hour") as? String) != nil) {
            hourTxtFld.text = UserDefaults.standard.object(forKey: "hour") as? String
        }
        
        if ((UserDefaults.standard.object(forKey: "minute") as? String) != nil) {
            minuteTxtFld.text = UserDefaults.standard.object(forKey: "minute") as? String
        }
    }
    
    
    /*
    //MARK: Get Premission for access Calender
    func SOGetPermissionCalendarAccess() {
        switch EKEventStore.authorizationStatus(for: .event) {
        case .authorized:
            print("Authorised")
        case .denied:
            print("Access denied")
        case .notDetermined:
            // 3
            eventStore.requestAccess(to: .event, completion: { (granted: Bool, error:NSError?) -> Void in
                if granted {
                    print("Granted")
                } else {
                    print("Access Denied")
                }
                } as! EKEventStoreRequestAccessCompletionHandler)
        default:
            print("Case Default")
        }
    }
    */
    
    @IBAction func selectOptionAction(_ sender: UIButton) {
        
        
        btnCalendar.isSelected = false
        
        if sender.isSelected {
            sender.isSelected = false
            self.tableViewList.isHidden = true
        }
        else{
            sender.isSelected = true
            self.tableViewList.isHidden = false
            self.tableViewList.reloadData()
            
            calendarView.isHidden = true
            
            calendarView.dataSource = nil
            calendarView.delegate = nil
        }
        
    }
    
    @IBAction func selectDateAction(_ sender: UIButton) {
        
        self.tableViewList.isHidden = true
        self.view.endEditing(true)

        
        if sender.isSelected {
            sender.isSelected = false
            calendarView.isHidden = true
            
            calendarView.dataSource = nil
            calendarView.delegate = nil
        }
        else{
            
            if lblOption.text != "Option wheel Clinic visit, cast removal, wound care, surgery etc..."
            {
                calendarView.dataSource = self
                calendarView.delegate = self
                
                sender.isSelected = true
                calendarView.isHidden = false
                
                let today = Date()
                
                var tomorrowComponents = DateComponents()
                tomorrowComponents.day = 1
                
                
                //        let tomorrow = self.calendarView.calendar.date(byAdding: tomorrowComponents, to: today)!
                self.calendarView.selectDate(today)
                
                self.calendarView.loadEvents() { error in
                    if error != nil {
                        let message = "The BreakItDown could not load system events. It is possibly a problem with permissions"
                        let alert = UIAlertController(title: "Events Loading Error", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
                
                self.calendarView.setDisplayDate(today)
                //        self.datePicker.setDate(today, animated: false)
            }
            else{
                
                let alert = UIAlertController(title: "Alert", message: "Please select appointment", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                        
                        
                    }}))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        var celll:UITableViewCell?
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.selectionCell, for: indexPath) as! SelectionTableViewCell
        cell.selectionStyle = .none
        cell.updateCell(arrayWithOption[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UIDevice.current.userInterfaceIdiom != .pad {
            
            lblOption.text = arrayWithOption[indexPath.row]
            
            btnActionData.isSelected = false
            self.tableViewList.isHidden = true

            UserDefaults.standard.set(lblOption.text, forKey: "appointmentSubject")

        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    // MARK : KDCalendarDataSource
    
    func startDate() -> Date {
        
        var dateComponents = DateComponents()
        dateComponents.month = -3
        
        let today = Date()
        
        let threeMonthsAgo = self.calendarView.calendar.date(byAdding: dateComponents, to: today)!
        
        return threeMonthsAgo
    }
    
    func endDate() -> Date {
        
        var dateComponents = DateComponents()
        
        dateComponents.year = 2;
        let today = Date()
        
        let twoYearsFromNow = self.calendarView.calendar.date(byAdding: dateComponents, to: today)!
        
        return twoYearsFromNow
        
    }
    
    
    // MARK : KDCalendarDelegate
    
    func calendar(_ calendar: CalendarView, didSelectDate date : Date, withEvents events: [CalendarEvent]) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd,yyyy"
        dateS = dateFormatter.string(from: date)
        
        lblDate.text = dateS

//        UserDefaults.standard.set(dateS, forKey: "date")

        hourTxtFld.isUserInteractionEnabled = true
        minuteTxtFld.isUserInteractionEnabled = true
        saveBtn.isUserInteractionEnabled = true
        
        /*
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd hh:mm"
        let someDateTime = formatter.date(from: "2018/10/26 12:30")
        */
        
        
        print("Did Select: \(date) with \(events.count) events")
        for event in events {
            print("\t\"\(event.title)\" - Starting at:\(event.startDate)")
        }
        
        let formatterNew = DateFormatter()
        
        let calendar = Calendar.current
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
        components.timeZone = TimeZone(abbreviation: "UTC")
        
        let finalDate = calendar.date(from:components)
        
        let todoItem = TodoItem(deadline: finalDate!, title: lblOption.text!, UUID: UUID().uuidString)
        TodoList.sharedInstance.addItem(todoItem) // schedule a local notification to persist this item
        
        
        //------------------------------------------------------------------

        let center = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .sound];

        // Swift
        center.requestAuthorization(options: options) {
            (granted, error) in
            if !granted {
                print("Something went wrong")
            }
        }

        // Swift
        center.getNotificationSettings { (settings) in
            if settings.authorizationStatus != .authorized {
                // Notifications not allowed
            }
        }
        
        // Swift
        let content = UNMutableNotificationContent()
        content.title = "Don't forget"
        content.body = lblOption.text!
        content.sound = UNNotificationSound.default()
        
        
        let dateNew = Date(timeInterval: 100, since: date)
    
        
        //--------------------------------------------------------------------
       
        
        savedDate = date
        
        /*
        let gregorian = Calendar(identifier: .gregorian)

        var components2 = gregorian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
        
        components2.hour = 10
        components2.minute = 30
        components2.second = 0
        
        /*
        components2.hour = 10
        components2.minute = 30
        components2.second = 0
        */
        
        let date2 = gregorian.date(from: components2)!
        
        
        let triggerDaily = Calendar.current.dateComponents([.hour,.minute,.second,], from: date2)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
        

        // Swift
        let identifier = "UYLLocalNotification"
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: trigger)
        
        center.add(request, withCompletionHandler: { (error) in
            if let error = error {
                // Something went wrong
            }
        })

        
        // Swift
        let snoozeAction = UNNotificationAction(identifier: "Snooze",
                                                title: "Snooze", options: [])
        let deleteAction = UNNotificationAction(identifier: "UYLDeleteAction",
                                                title: "Delete", options: [.destructive])
        
        // Swift
        let category = UNNotificationCategory(identifier: "UYLReminderCategory",
                                              actions: [snoozeAction,deleteAction],
                                              intentIdentifiers: [], options: [])
        
        // Swift
        center.setNotificationCategories([category])
        
        content.categoryIdentifier = "UYLReminderCategory"
        */
        
        //------------------------------------------------------------------

        
        hourSelected = 0
        minuteSelected = 0
        
        hourTxtFld.text = ""
        minuteTxtFld.text = ""
        
    }
    
    func calendar(_ calendar: CalendarView, didScrollToMonth date : Date) {
        
        
        /*
//        self.deadlinePicker.setDate(date, animated: true)
        
        let todoItem = TodoItem(deadline: self.deadlinePicker.date, title: "", UUID: UUID().uuidString)
        TodoList.sharedInstance.addItem(todoItem) // schedule a local notification to persist this item
        */
        
    }
    
    
    func calendar(_ calendar: CalendarView, didLongPressDate date : Date) {
        
        let alert = UIAlertController(title: "Create New Event", message: "Message", preferredStyle: .alert)
        
        alert.addTextField { (textField: UITextField) in
            textField.placeholder = "Event Title"
        }
        
        let addEventAction = UIAlertAction(title: "Create", style: .default, handler: { (action) -> Void in
            let title = alert.textFields?.first?.text
            self.calendarView.addEvent(title!, date: date)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alert.addAction(addEventAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    
    
    // MARK : Events
    
    @IBAction func onValueChange(_ picker : UIDatePicker) {
        self.calendarView.setDisplayDate(picker.date, animated: true)
    }
    
    @IBAction func goToPreviousMonth(_ sender: Any) {
        self.calendarView.goToPreviousMonth()
    }
    @IBAction func goToNextMonth(_ sender: Any) {
        self.calendarView.goToNextMonth()
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func backAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "removeTabbarView"), object: nil)
    }
    
    
    @IBAction func backToHome(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "removeTabbarView"), object: nil)
    }
    
    @IBAction func menuAction(_ sender: Any) {
        
        if isSelect == false
        {
            isSelect = true
            menuOptionView.isHidden = false
        }
        else{
            isSelect = false
            menuOptionView.isHidden = true
        }
    }

    @IBAction func faqAction(_ sender: UIButton) {
        
        isSelect = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FaqViewController") as? FaqViewController
        self.present(vc!, animated: false) {
            
        }
        
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        viewFaq.layer.add(transition, forKey: "animation")
        self.viewFaq.isHidden = false
        */
    }
    
    @IBAction func notesAction(_ sender: Any) {
        
        isSelect = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NotesViewController") as? NotesViewController
        self.present(vc!, animated: false) {
            
        }
        
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        notesView.layer.add(transition, forKey: "animation")
        self.notesView.isHidden = false
        
        notesTextView.text = UserDefaults.standard.object(forKey: "notes") as? String
        */
    }
    
    
    @IBAction func selectTimeAction(_ sender: UIButton) {
        
//        deadlinePicker.isHidden = false
        
//        deadlinePicker.datePickerMode = UIDatePickerMode.time
        // datePicker.backgroundColor = UIColor.blue
//        print(deadlinePicker)
//        txtFildDeadLine.textField.inputView = datePicker
    }
    
    
    func textFieldDidChange(_ textField: UITextField) {
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        
        if newLength < 3 {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            
            var updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            
            if textField.tag == 1 {
                if updatedText != ""
                {
                    
                    let a:Int? = Int(updatedText) // firstText is UITextField
                    
                    hourSelected = a!
                    
                    if a! > 23
                    {
                        let a1:Int? = Int(currentText) // firstText is UITextField
                        hourSelected = a1!
                        
                        
                        return false
                    }
                }
                
            }
            else if textField.tag == 2
            {
                if updatedText != ""
                {
                    
                    let b:Int? = Int(updatedText) // secondText is UITextField
                    minuteSelected = b!
                    
                    if b! > 60
                    {
                        let b1:Int? = Int(currentText) // secondText is UITextField
                        minuteSelected = b1!
                        
                        return false
                    }
                }
            }
            
            return updatedText.count <= 2
        }
        
        // return NO to not change text
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.view.addGestureRecognizer(tapRecognizer)

        /*
        if !(textField.tag == 1 || textField.tag == 3 || textField.tag == 5)
        {
            if textField.tag == 15 || textField.tag == 17
            {
                self.view.frame = CGRect(x: 0, y: -200, width: self.view.frame.width, height: self.view.frame.height)
            }
            else{
                
                self.view.frame = CGRect(x: 0, y: -150, width: self.view.frame.width, height: self.view.frame.height)
            }
        }
        */
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        self.view.endEditing(true)
        
//        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


/*
class UYLNotificationDelegate: NSObject, UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Play sound and show alert to the user
        completionHandler([.alert,.sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        // Determine the user action
        switch response.actionIdentifier {
        case UNNotificationDismissActionIdentifier:
            print("Dismiss Action")
        case UNNotificationDefaultActionIdentifier:
            print("Default")
        case "Snooze":
            print("Snooze")
        case "Delete":
            print("Delete")
        default:
            print("Unknown action")
        }
        completionHandler()
    }
}
*/


