//
//  MapDirectionViewController.swift
//  BreakItDown
//
//  Created by Dobango on 08/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit

class MapDirectionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {

    @IBOutlet weak var mapBtn: UIButton!
    @IBOutlet weak var directionBtn: UIButton!
    @IBOutlet weak var parkingBtn: UIButton!
    
    @IBOutlet weak var menuOptionView: UIView!
    var isSelect: Bool = false
    
    @IBOutlet weak var tableViewList: UITableView!
    var innerTabType = ""
    
    var addingCell:Bool = false
    var selectedIndex: Int?
    
    var mapAddressArrayCount = 4
    var mapDirectionsArrayCount = 8
    var mapParkingArrayCount = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableViewList.register(UINib(nibName: CellIdentifiers.anatomyCell1, bundle: nil), forCellReuseIdentifier: CellIdentifiers.anatomyCell1)
        tableViewList.register(UINib(nibName: CellIdentifiers.anatomyCell2, bundle: nil), forCellReuseIdentifier: CellIdentifiers.anatomyCell2)
        tableViewList.register(UINib(nibName: CellIdentifiers.anatomyCell4, bundle: nil), forCellReuseIdentifier: CellIdentifiers.anatomyCell4)

        tableViewList.register(UINib(nibName: CellIdentifiers.mapViewCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.mapViewCell)
        tableViewList.register(UINib(nibName: CellIdentifiers.navigateButtonCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.navigateButtonCell)
        tableViewList.register(UINib(nibName: CellIdentifiers.addressPhoneCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.addressPhoneCell)

        
        tableViewList.estimatedRowHeight = 500
        tableViewList.rowHeight = UITableViewAutomaticDimension
        
        innerTabType = "MAP"
        self.reloadContents()
        
        mapBtn.setTitleColor(Color.Orange.Medium, for: .normal)
        mapBtn.backgroundColor = UIColor.white
        
        directionBtn.setTitleColor(UIColor.white, for: .normal)
        directionBtn.backgroundColor = Color.Orange.Medium
        
        parkingBtn.setTitleColor(UIColor.white, for: .normal)
        parkingBtn.backgroundColor = Color.Orange.Medium
        
        mapBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 19)
        directionBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        parkingBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        
    }

    func reloadContents() {
        tableViewList.delegate = self
        tableViewList.dataSource = self
        tableViewList.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if innerTabType == "MAP"
        {
            return mapAddressArrayCount
        }
        else if innerTabType == "DIRECTIONS"
        {
            return mapDirectionsArrayCount
        }
        else if innerTabType == "PARKING"
        {
            return mapParkingArrayCount
        }
        
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var celll:UITableViewCell?
        
        if innerTabType == "MAP" {
            
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.labelSummary.textAlignment = .center
                cell.labelSummary!.font = UIFont(name: RobotoFontName.RobotoCondensedBold.rawValue, size: 17)
                cell.updateCell("UCSF Benioff Children’s Hospital")
                return cell
            }
            else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.addressPhoneCell, for: indexPath) as! AddressPhoneTableViewCell
                cell.selectionStyle = .none
                cell.labelSummary.textAlignment = .left
                cell.labelSummary!.font = UIFont(name: RobotoFontName.RobotoCondensedBold.rawValue, size: 17)
                cell.btnPhoneCall!.titleLabel?.font = UIFont(name: RobotoFontName.RobotoCondensedBold.rawValue, size: 17)

                cell.updateCell(localDataClavicle.MyMapAddress1)
                return cell
            }
            else if indexPath.row == 2  {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.navigateButtonCell, for: indexPath) as! NavigateButtonCell
                
                return cell
//                return cell.contentView.bounds.height
                /*
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("Navigate", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.clavicleRecovery
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                
                return cell
                */
            }
            else if indexPath.row == 3 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.mapViewCell, for: indexPath) as! MapCell
                cell.selectionStyle = .none
//                let imageName = "hospitalAddressBuilding.png"
//                let image = UIImage(named: imageName)
//                cell.updateCell(image!)
                return cell
                
                /*
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell4, for: indexPath) as! AnatomyTableViewCell4
                cell.selectionStyle = .none
                let imageName = "hospitalAddressBuilding.png"
                let image = UIImage(named: imageName)
                cell.updateCell(image!)
                return cell
                */
            }
        }
        else if innerTabType == "DIRECTIONS" {
            
            if indexPath.row == 0  {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell4, for: indexPath) as! AnatomyTableViewCell4
                cell.selectionStyle = .none
                let imageName = "hospitalAddressBuilding.png"
                let image = UIImage(named: imageName)
                cell.updateCell(image!)
                return cell
            }
            else if indexPath.row == 1 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell(localDataClavicle.MyMapDirectionTitle)
                return cell
            }
            else if indexPath.row == 2 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.labelSummary.textAlignment = .center
                cell.updateCell("Driving Directions")
                return cell
            }
            else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("Public Transportation", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapDrivingDirection1
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("From East Bay", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapDrivingDirection2
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 5 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("From Marin County", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapDrivingDirection3
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 6 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("From SFO/South Bay (101)", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapDrivingDirection4
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 7 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("From The Peninsula I-280", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapDrivingDirection5
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
        }
        else if innerTabType == "PARKING" {
            
            if indexPath.row == 0  {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell4, for: indexPath) as! AnatomyTableViewCell4
                cell.selectionStyle = .none
                let imageName = "hospitalAddressBuilding.png"
                let image = UIImage(named: imageName)
                cell.updateCell(image!)
                return cell
            }
            else if indexPath.row == 1 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell2, for: indexPath) as! AnatomyTableViewCell2
                cell.selectionStyle = .none
                cell.updateCell("Parking at Mission Bay")
                return cell
            }
            else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("Parking garage", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyParking1
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("Valet parking", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyParking2
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("Disabled parking", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyParking3
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            
        }
        
        
        return celll!
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UIDevice.current.userInterfaceIdiom != .pad {
            
            if innerTabType == "MAP"
            {
                
            }
            else if innerTabType == "DIRECTIONS"
            {
                if indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 7
                {
                    if addingCell == false
                    {
                        addingCell = true
                    }
                    else{
                        addingCell = false
                    }
                    tableView.reloadData()
                    selectedIndex = indexPath.row
                    scrollTableViewToRow(indexPath.row)
                }
            }
            else if innerTabType == "PARKING"
            {
                if addingCell == false
                {
                    addingCell = true
                }
                else{
                    addingCell = false
                }
                tableView.reloadData()
                selectedIndex = indexPath.row
                scrollTableViewToRow(indexPath.row)
            }
        }
    }
    
    func scrollTableViewToRow(_ row:Int) {
        let seconds = 0.1
        let delay = seconds * Double(NSEC_PER_SEC)  // nanoseconds per seconds
        let dispatchTime = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.tableViewList.scrollToRow(at: IndexPath(row: row, section: 0), at: UITableViewScrollPosition.top, animated: false)
        })
        
    }
    
    
    
    @IBAction func mapTabAction(_ sender: Any) {
        innerTabType = "MAP"
        addingCell = false
        self.reloadContents()
        
        mapBtn.setTitleColor(Color.Orange.Medium, for: .normal)
        mapBtn.backgroundColor = UIColor.white
        
        directionBtn.setTitleColor(UIColor.white, for: .normal)
        directionBtn.backgroundColor = Color.Orange.Medium
        
        parkingBtn.setTitleColor(UIColor.white, for: .normal)
        parkingBtn.backgroundColor = Color.Orange.Medium
        
        scrollTableViewToRow(0)
        
        mapBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 19)
        directionBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        parkingBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)

    }
    
    @IBAction func directionTabAction(_ sender: Any) {
        innerTabType = "DIRECTIONS"
        addingCell = false
        self.reloadContents()
        
        mapBtn.setTitleColor(UIColor.white, for: .normal)
        mapBtn.backgroundColor = Color.Orange.Medium
        
        directionBtn.setTitleColor(Color.Orange.Medium, for: .normal)
        directionBtn.backgroundColor = UIColor.white
        
        parkingBtn.setTitleColor(UIColor.white, for: .normal)
        parkingBtn.backgroundColor = Color.Orange.Medium
        
        scrollTableViewToRow(0)
        
        mapBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        directionBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 19)
        parkingBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)

    }
    
    @IBAction func parkingTabAction(_ sender: Any) {
        innerTabType = "PARKING"
        addingCell = false
        self.reloadContents()
        
        mapBtn.setTitleColor(UIColor.white, for: .normal)
        mapBtn.backgroundColor = Color.Orange.Medium
        
        directionBtn.setTitleColor(UIColor.white, for: .normal)
        directionBtn.backgroundColor = Color.Orange.Medium
        
        parkingBtn.setTitleColor(Color.Orange.Medium, for: .normal)
        parkingBtn.backgroundColor = UIColor.white
        
        scrollTableViewToRow(0)
        
        mapBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        directionBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        parkingBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 19)

    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    @IBAction func backToHome(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "removeTabbarView"), object: nil)
    }
    
   
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuAction(_ sender: Any) {
        
        if isSelect == false
        {
            isSelect = true
            menuOptionView.isHidden = false
        }
        else{
            isSelect = false
            menuOptionView.isHidden = true
        }
    }
    
    @IBAction func faqAction(_ sender: UIButton) {
        
        isSelect = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FaqViewController") as? FaqViewController
        self.present(vc!, animated: false) {
            
        }
        
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        viewFaq.layer.add(transition, forKey: "animation")
        self.viewFaq.isHidden = false
        */
    }
    
    @IBAction func notesAction(_ sender: Any) {
        
        isSelect = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NotesViewController") as? NotesViewController
        self.present(vc!, animated: false) {
            
        }

        
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        notesView.layer.add(transition, forKey: "animation")
        self.notesView.isHidden = false
        
        notesTextView.text = UserDefaults.standard.object(forKey: "notes") as? String
        */
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
