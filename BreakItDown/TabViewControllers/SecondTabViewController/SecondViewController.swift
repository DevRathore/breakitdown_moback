//
//  SecondViewController.swift
//  BreakItDown
//
//  Created by Dobango on 05/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit
import Firebase


class SecondViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {

    
    @IBOutlet weak var mapBtn: UIButton!
    @IBOutlet weak var parkingBtn: UIButton!
    
    @IBOutlet weak var contactUsBtn: UIButton!
    
    
    @IBOutlet weak var tableViewList: UITableView!
    var innerTabType = ""
    
    var addingCell:Bool = false
    var selectedIndex: Int?
    
    var mapAddressArrayCount = 6
    var mapParkingArrayCount = 6
    var mapContactsArrayCount = 6
    
    @IBOutlet weak var menuOptionView: UIView!
    
    var isSelect: Bool = false
    
    override func viewDidAppear(_ animated: Bool) {
        
//        FirebaseApp.configure()

        //------------ MapNew --------------------------------------------
        let refMapAddress = Database.database().reference().child("MAPNEW")
        refMapAddress.observe(DataEventType.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                for objectData in snapshot.children.allObjects as! [DataSnapshot] {
                    //getting values
                    let objectValue = objectData.value as? [String: AnyObject]
                    
                    let addressTitle: String = objectValue?["addressTitle"] as! String
                    let addressSummary: String = objectValue?["addressSummary"] as! String
                    let parkingDetail: String = objectValue?["parkingDetail"] as! String
                    
                    let addressDirections: [String: AnyObject]  = objectValue?["Directions"] as! [String : AnyObject]
                    let addressParking: [String: AnyObject]  = objectValue?["Parking"] as! [String : AnyObject]
                    
                    
                    if objectData.key == "Address1"
                    {
                        localDataClavicle.Address1 = addressTitle
                        localDataClavicle.MyMapAddress1 = addressSummary
                        
                        localDataClavicle.MyMapAddress1_Parking = parkingDetail
                        
                        localDataClavicle.MyMapDirectionTitle = addressDirections["content"] as! String
                        
                        localDataClavicle.MyMapDrivingDirection1 = addressDirections["subtitle1"] as! String
                        localDataClavicle.MyMapDrivingDirection2 = addressDirections["subtitle2"] as! String
                        localDataClavicle.MyMapDrivingDirection3 = addressDirections["subtitle3"] as! String
                        localDataClavicle.MyMapDrivingDirection4 = addressDirections["subtitle4"] as! String
                        localDataClavicle.MyMapDrivingDirection5 = addressDirections["subtitle5"] as! String
                        
                        localDataClavicle.MyParking1 = addressParking["myparking1"] as! String
                        localDataClavicle.MyParking2 = addressParking["myparking2"] as! String
                        localDataClavicle.MyParking3 = addressParking["myparking3"] as! String
                        
                    }
                    else if objectData.key == "Address2"
                    {
                        localDataClavicle.Address2 = addressTitle
                        localDataClavicle.MyMapAddress2 = addressSummary
                        
                        localDataClavicle.MyMapAddress2_Parking = parkingDetail
                        
                        localDataClavicle.MyMapDirectionTitle = addressDirections["content"] as! String
                        
                        localDataClavicle.MyMapDriving2Direction1 = addressDirections["subtitle1"] as! String
                        localDataClavicle.MyMapDriving2Direction2 = addressDirections["subtitle2"] as! String
                        localDataClavicle.MyMapDriving2Direction3 = addressDirections["subtitle3"] as! String
                        localDataClavicle.MyMapDriving2Direction4 = addressDirections["subtitle4"] as! String
                        localDataClavicle.MyMapDriving2Direction5 = addressDirections["subtitle5"] as! String
                        
                        localDataClavicle.MyParking1 = addressParking["myparking1"] as! String
                        localDataClavicle.MyParking2 = addressParking["myparking2"] as! String
                        localDataClavicle.MyParking3 = addressParking["myparking3"] as! String
                        
                    }
                    else if objectData.key == "Address3"
                    {
                        localDataClavicle.Address3 = addressTitle
                        localDataClavicle.MyMapAddress3 = addressSummary
                        
                        localDataClavicle.MyMapAddress3_Parking = parkingDetail
                        
                        localDataClavicle.MyMapDirectionTitle = addressDirections["content"] as! String
                        
                        localDataClavicle.MyMapDriving3Direction1 = addressDirections["subtitle1"] as! String
                        localDataClavicle.MyMapDriving3Direction2 = addressDirections["subtitle2"] as! String
                        localDataClavicle.MyMapDriving3Direction3 = addressDirections["subtitle3"] as! String
                        localDataClavicle.MyMapDriving3Direction4 = addressDirections["subtitle4"] as! String
                        localDataClavicle.MyMapDriving3Direction5 = addressDirections["subtitle5"] as! String
                        
                        localDataClavicle.MyParking1 = addressParking["myparking1"] as! String
                        localDataClavicle.MyParking2 = addressParking["myparking2"] as! String
                        localDataClavicle.MyParking3 = addressParking["myparking3"] as! String
                        
                    }
                    else if objectData.key == "Address4"
                    {
                        localDataClavicle.Address4 = addressTitle
                        localDataClavicle.MyMapAddress4 = addressSummary
                        
                        localDataClavicle.MyMapAddress4_Parking = parkingDetail
                        
                        localDataClavicle.MyMapDirectionTitle = addressDirections["content"] as! String
                        
                        localDataClavicle.MyMapDriving4Direction1 = addressDirections["subtitle1"] as! String
                        localDataClavicle.MyMapDriving4Direction2 = addressDirections["subtitle2"] as! String
                        localDataClavicle.MyMapDriving4Direction3 = addressDirections["subtitle3"] as! String
                        localDataClavicle.MyMapDriving4Direction4 = addressDirections["subtitle4"] as! String
                        localDataClavicle.MyMapDriving4Direction5 = addressDirections["subtitle5"] as! String
                        
                        localDataClavicle.MyParking1 = addressParking["myparking1"] as! String
                        localDataClavicle.MyParking2 = addressParking["myparking2"] as! String
                        localDataClavicle.MyParking3 = addressParking["myparking3"] as! String
                        
                    }
                    else if objectData.key == "Address5"
                    {
                        localDataClavicle.Address5 = addressTitle
                        localDataClavicle.MyMapAddress5 = addressSummary
                        
                        localDataClavicle.MyMapAddress5_Parking = parkingDetail
                        
                        localDataClavicle.MyMapDirectionTitle = addressDirections["content"] as! String
                        
                        localDataClavicle.MyMapDriving5Direction1 = addressDirections["subtitle1"] as! String
                        localDataClavicle.MyMapDriving5Direction2 = addressDirections["subtitle2"] as! String
                        localDataClavicle.MyMapDriving5Direction3 = addressDirections["subtitle3"] as! String
                        localDataClavicle.MyMapDriving5Direction4 = addressDirections["subtitle4"] as! String
                        localDataClavicle.MyMapDriving5Direction5 = addressDirections["subtitle5"] as! String
                        
                        localDataClavicle.MyParking1 = addressParking["myparking1"] as! String
                        localDataClavicle.MyParking2 = addressParking["myparking2"] as! String
                        localDataClavicle.MyParking3 = addressParking["myparking3"] as! String
                        
                    }
                }
            }
        })
        //---------------------------------------------------------------------
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableViewList.layer.backgroundColor = UIColor.clear.cgColor

        
        tableViewList.register(UINib(nibName: CellIdentifiers.anatomyCell1, bundle: nil), forCellReuseIdentifier: CellIdentifiers.anatomyCell1)

        tableViewList.estimatedRowHeight = 100
        tableViewList.rowHeight = UITableViewAutomaticDimension
        
        innerTabType = "MAP"
        self.reloadContents()
        
        mapBtn.setTitleColor(Color.Orange.Medium, for: .normal)
        mapBtn.backgroundColor = UIColor.white
        
        parkingBtn.setTitleColor(UIColor.white, for: .normal)
        parkingBtn.backgroundColor = Color.Orange.Medium
        
        contactUsBtn.setTitleColor(UIColor.white, for: .normal)
        contactUsBtn.backgroundColor = Color.Orange.Medium
        
        
        mapBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 19)
        parkingBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        contactUsBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "removeTabbarView"), object: nil)
    }
    
    
    @IBAction func backToHomeAction(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "removeTabbarView"), object: nil)
    }
    
    
    
    @IBAction func mapTabAction(_ sender: Any) {
        innerTabType = "MAP"
        addingCell = false
        self.reloadContents()
        
        mapBtn.setTitleColor(Color.Orange.Medium, for: .normal)
        mapBtn.backgroundColor = UIColor.white
        
        parkingBtn.setTitleColor(UIColor.white, for: .normal)
        parkingBtn.backgroundColor = Color.Orange.Medium
        
        contactUsBtn.setTitleColor(UIColor.white, for: .normal)
        contactUsBtn.backgroundColor = Color.Orange.Medium
        
        scrollTableViewToRow(0)
        
        
        mapBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 19)
        parkingBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        contactUsBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
    }
    
    @IBAction func parkingTabAction(_ sender: Any) {
        innerTabType = "PARKING"
        addingCell = false
        self.reloadContents()
        
        mapBtn.setTitleColor(UIColor.white, for: .normal)
        mapBtn.backgroundColor = Color.Orange.Medium
        
        parkingBtn.setTitleColor(Color.Orange.Medium, for: .normal)
        parkingBtn.backgroundColor = UIColor.white
        
        contactUsBtn.setTitleColor(UIColor.white, for: .normal)
        contactUsBtn.backgroundColor = Color.Orange.Medium
        
        scrollTableViewToRow(0)

        mapBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        parkingBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 19)
        contactUsBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
    }
    
    @IBAction func contactUsTabAction(_ sender: Any) {
        innerTabType = "CONTACTUS"
        addingCell = false
        self.reloadContents()
        
        mapBtn.setTitleColor(UIColor.white, for: .normal)
        mapBtn.backgroundColor = Color.Orange.Medium
        
        parkingBtn.setTitleColor(UIColor.white, for: .normal)
        parkingBtn.backgroundColor = Color.Orange.Medium
        
        contactUsBtn.setTitleColor(Color.Orange.Medium, for: .normal)
        contactUsBtn.backgroundColor = UIColor.white
        
        scrollTableViewToRow(0)

        mapBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        parkingBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        contactUsBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 19)
    }
    
    func reloadContents() {
        tableViewList.delegate = self
        tableViewList.dataSource = self
        tableViewList.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if innerTabType == "MAP"
        {
            return mapAddressArrayCount
        }
        else if innerTabType == "PARKING"
        {
            return mapParkingArrayCount
        }
        else if innerTabType == "CONTACTUS"
        {
            return mapContactsArrayCount
        }
        
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var celll:UITableViewCell?
        
        if innerTabType == "MAP" {
            
            if indexPath.row == 0  {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
//                cell.layer.backgroundColor = UIColor.clear.cgColor
                cell.label.textAlignment = .left

                cell.updateCell("UCSF Benioff Children’s Hospital - San Francisco", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapAddress1
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                
                
                return cell
            }
            else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
//                cell.layer.backgroundColor = UIColor.clear.cgColor
                cell.label.textAlignment = .left

                cell.updateCell("UCSF Benioff Children’s Hospital - Oakland", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapAddress2
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
//                cell.layer.backgroundColor = UIColor.clear.cgColor
                cell.label.textAlignment = .left

                cell.updateCell("UCSF Benioff Children’s Hospital - Walnut Creek", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapAddress3
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
//                cell.layer.backgroundColor = UIColor.clear.cgColor
                cell.label.textAlignment = .left

                cell.updateCell("UCSF Benioff Children’s Hospital - San Francisco", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapAddress4
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
//                cell.layer.backgroundColor = UIColor.clear.cgColor
                cell.label.textAlignment = .left

                cell.updateCell("Zuckerberg San Francisco General Hospital", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapAddress5
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 5 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
//                cell.layer.backgroundColor = UIColor.clear.cgColor
                cell.label.textAlignment = .left

                cell.updateCell("Marin Greenbrae Care Clinic", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapAddress6
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
        }
        else if innerTabType == "PARKING" {
            
            if indexPath.row == 0  {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("UCSF Benioff Children’s Hospital - San Francisco", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapAddress1_Parking
                    
                    let strStr1 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking1), boldString: "Parking garage:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)
                    
                    let strStr2 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking2), boldString: "Valet parking:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)
                    
                    let strStr3 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking3), boldString: "Disabled parking:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)
                    
                    let combination = NSMutableAttributedString()
                    
                    combination.append(strStr1)
                    combination.append(strStr2)
                    combination.append(strStr3)
                    
                    cell.data.attributedText = combination
                    
                    cell.contentView.setNeedsLayout()
                    
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                
                return cell
            }
            else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("UCSF Benioff Children’s Hospital - Oakland", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapAddress2_Parking
                    
                    let strStr1 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking1), boldString: "Parking garage:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)
                    
                    let strStr2 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking2), boldString: "Valet parking:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)
                    
                    let strStr3 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking3), boldString: "Disabled parking:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)
                    
                    let combination = NSMutableAttributedString()
                    
                    combination.append(strStr1)
                    combination.append(strStr2)
                    combination.append(strStr3)
                    
                    cell.data.attributedText = combination
                    
                    
                    cell.contentView.setNeedsLayout()
                    
                    
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("UCSF Benioff Children’s Hospital - Walnut Creek", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapAddress3_Parking
                    
                    let strStr1 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking1), boldString: "Parking garage:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)
                    
                    let strStr2 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking2), boldString: "Valet parking:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)
                    
                    let strStr3 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking3), boldString: "Disabled parking:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)
                    
                    let combination = NSMutableAttributedString()
                    
                    combination.append(strStr1)
                    combination.append(strStr2)
                    combination.append(strStr3)
                    
                    cell.data.attributedText = combination
                    
                    cell.contentView.setNeedsLayout()
                    
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("UCSF Benioff Children’s Hospital - San Francisco", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapAddress4_Parking
                    
                    let strStr1 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking1), boldString: "Parking garage:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)
                    
                    let strStr2 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking2), boldString: "Valet parking:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)
                    
                    let strStr3 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking3), boldString: "Disabled parking:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)
                    
                    let combination = NSMutableAttributedString()
                    
                    combination.append(strStr1)
                    combination.append(strStr2)
                    combination.append(strStr3)
                    
                    cell.data.attributedText = combination
                    
                    cell.contentView.setNeedsLayout()
                    
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("Zuckerberg San Francisco General Hospital", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapAddress5_Parking
                    
                    let strStr1 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking1), boldString: "Parking garage:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)
                    
                    let strStr2 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking2), boldString: "Valet parking:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)
                    
                    let strStr3 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking3), boldString: "Disabled parking:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)
                    
                    let combination = NSMutableAttributedString()
                    
                    combination.append(strStr1)
                    combination.append(strStr2)
                    combination.append(strStr3)
                    
                    cell.data.attributedText = combination
                    
                    
                    cell.contentView.setNeedsLayout()
                    
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 5 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("Marin Greenbrae Care Clinic", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    
                    
                    cell.data.text = localDataClavicle.MyMapAddress6_Parking
                    
                    let strStr1 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking1), boldString: "Parking garage:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)

                    let strStr2 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking2), boldString: "Valet parking:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)
                    
                    let strStr3 = attributedText(withString: String(format: "%@", localDataClavicle.testContent1_Parking3), boldString: "Disabled parking:", font: UIFont(name: RobotoFontName.RobotoCondensedRegular.rawValue, size: 16)!)
                    
                    let combination = NSMutableAttributedString()
                    
                    combination.append(strStr1)
                    combination.append(strStr2)
                    combination.append(strStr3)
                    
                    cell.data.attributedText = combination
                    
                    cell.contentView.setNeedsLayout()
                    
                    
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
        }
        else if innerTabType == "CONTACTUS" {
            
            if indexPath.row == 0  {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("UCSF Benioff Children’s Hospital - San Francisco", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapAddress1
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                
                return cell
            }
            else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("UCSF Benioff Children’s Hospital - Oakland", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapAddress2
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("UCSF Benioff Children’s Hospital - Walnut Creek", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapAddress3
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("UCSF Benioff Children’s Hospital - San Francisco", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapAddress4
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("Zuckerberg San Francisco General Hospital", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapAddress5
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
            else if indexPath.row == 5 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.updateCell("Marin Greenbrae Care Clinic", color1: Color.Orange.Medium, color2: Color.Orange.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.MyMapAddress6
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
        }
        
        
        return celll!

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UIDevice.current.userInterfaceIdiom != .pad {
            
            if innerTabType == "MAP"
            {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MapDirectionViewController") as? MapDirectionViewController
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if innerTabType == "PARKING"
            {
                if addingCell == false
                {
                    addingCell = true
                }
                else{
                    addingCell = false
                }
                tableView.reloadData()
                selectedIndex = indexPath.row
                scrollTableViewToRow(indexPath.row)
            }
            else if innerTabType == "CONTACTUS"
            {
                if addingCell == false
                {
                    addingCell = true
                }
                else{
                    addingCell = false
                }
                tableView.reloadData()
                selectedIndex = indexPath.row
                scrollTableViewToRow(indexPath.row)
            }
        }
    }

    func scrollTableViewToRow(_ row:Int) {
        let seconds = 0.1
        let delay = seconds * Double(NSEC_PER_SEC)  // nanoseconds per seconds
        let dispatchTime = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.tableViewList.scrollToRow(at: IndexPath(row: row, section: 0), at: UITableViewScrollPosition.top, animated: true)
        })
    }
    
    @IBAction func menuAction(_ sender: Any) {
        
        if isSelect == false
        {
            isSelect = true
            menuOptionView.isHidden = false
        }
        else{
            isSelect = false
            menuOptionView.isHidden = true
        }
    }
    
    @IBAction func faqAction(_ sender: UIButton) {
        
        isSelect = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FaqViewController") as? FaqViewController
        self.present(vc!, animated: false) {
            
        }
        
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        viewFaq.layer.add(transition, forKey: "animation")
        self.viewFaq.isHidden = false
        */
    }
    
    @IBAction func notesAction(_ sender: Any) {
        
        isSelect = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NotesViewController") as? NotesViewController
        self.present(vc!, animated: false) {
            
        }

        
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        notesView.layer.add(transition, forKey: "animation")
        self.notesView.isHidden = false
        
        notesTextView.text = UserDefaults.standard.object(forKey: "notes") as? String
        */
    }
    
    func attributedText(withString string: String, boldString: String, font: UIFont) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string,
                                                         attributes: [NSAttributedStringKey.font: font])
        let boldFontAttribute: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: font.pointSize)]
        let range = (string as NSString).range(of: boldString)
        attributedString.addAttributes(boldFontAttribute, range: range)
        return attributedString
    }

}
