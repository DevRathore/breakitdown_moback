//
//  CastCrutchesViewController.swift
//  BreakItDown
//
//  Created by Dobango on 08/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit
import Firebase

class CastCrutchesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableViewList: UITableView!
    var addingCell:Bool = false

    var castCrutchesArrayCount = 2
    var selectedIndex: Int?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableViewList.register(UINib(nibName: CellIdentifiers.anatomyCell1, bundle: nil), forCellReuseIdentifier: CellIdentifiers.anatomyCell1)

        tableViewList.estimatedRowHeight = 500
        tableViewList.rowHeight = UITableViewAutomaticDimension
        
        //------------ Cast+CrutchesNew-------------------------------------
        
//        FirebaseApp.configure()

        let refCastCrutches = Database.database().reference().child("CASTCRUTCHESNEW")
        refCastCrutches.observe(DataEventType.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                for objectData in snapshot.children.allObjects as! [DataSnapshot] {
                    //getting values
                    let objectValue = objectData.value as? [String: AnyObject]
                    let objectTitle: String = objectValue?["title"] as! String
                    
                    let objectContent: String  = objectValue?["content"] as! String
                    
                    let objectSubtitle: [String: AnyObject]  = objectValue?["subtitle"] as! [String : AnyObject]
                    
                    
                    localDataClavicle.castDescription = objectSubtitle["Cast"] as! String
                    
                    localDataClavicle.crutchesDescription = objectSubtitle["Crutches"] as! String
                    
                    self.addingCell = false
                    self.scrollTableViewToRow(0)
                    
                }
            }
        })
        
        self.reloadContents()

        //---------------------------------------------------------------------
        
        
    }

    func scrollTableViewToRow(_ row:Int) {
        let seconds = 0.1
        let delay = seconds * Double(NSEC_PER_SEC)  // nanoseconds per seconds
        let dispatchTime = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.tableViewList.scrollToRow(at: IndexPath(row: row, section: 0), at: UITableViewScrollPosition.top, animated: false)
        })
        
    }
    
    func reloadContents() {
        tableViewList.delegate = self
        tableViewList.dataSource = self
        tableViewList.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return castCrutchesArrayCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var celll:UITableViewCell?
        
        if indexPath.row == 0  {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
            cell.selectionStyle = .none
            cell.updateCell("Cast", color1: Color.Blue.Medium, color2: Color.Blue.Light)
            if addingCell && selectedIndex == indexPath.row {
                cell.data.isHidden = false
                cell.data.text = localDataClavicle.castDescription
                cell.contentView.setNeedsLayout()
            } else {
                cell.data.isHidden = true
                cell.data.text = ""
                cell.contentView.setNeedsLayout()
            }
            
            return cell
        }
        else if indexPath.row == 1  {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
            cell.selectionStyle = .none
            cell.updateCell("Crutches", color1: Color.Blue.Medium, color2: Color.Blue.Light)
            if addingCell && selectedIndex == indexPath.row {
                cell.data.isHidden = false
                cell.data.text = localDataClavicle.crutchesDescription
                cell.contentView.setNeedsLayout()
            } else {
                cell.data.isHidden = true
                cell.data.text = ""
                cell.contentView.setNeedsLayout()
            }
            
            return cell
        }
        
        return celll!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UIDevice.current.userInterfaceIdiom != .pad {
            
            if addingCell == false
            {
                addingCell = true
            }
            else{
                addingCell = false
            }
            tableView.reloadData()
            selectedIndex = indexPath.row
            scrollTableViewToRow(indexPath.row)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
