//
//  InjuredLimbViewController.swift
//  BreakItDown
//
//  Created by Dobango on 08/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit
import Firebase

class InjuredLimbViewController: UIViewController {

    
    @IBOutlet weak var txtView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //------------ InjuredLimbNew-------------------------------------
        
//        FirebaseApp.configure()
        
        let refInjuredLimb = Database.database().reference().child("INJUREDLIMBNEW")
        refInjuredLimb.observe(DataEventType.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                for objectData in snapshot.children.allObjects as! [DataSnapshot] {
                    //getting values
                    let objectValue = objectData.value as? [String: AnyObject]
                    let objectTitle: String = objectValue?["title"] as! String
                    
                    let objectContent: String  = objectValue?["content"] as! String
                    
                    let objectSubtitle: [String: AnyObject]  = objectValue?["subtitle"] as! [String : AnyObject]
                    
                    localDataClavicle.injuredLimbDetails = objectContent
                    self.txtView.text = localDataClavicle.injuredLimbDetails
                    
                    //                    localDataClavicle.clavicleNonOperativeTreatment = objectSubtitle["Non-operative Treatment"] as! String
                    
                    //                    localDataClavicle.clavicleOperativeTreatment = objectSubtitle["Operative Treatment"] as! String
                    
                }
            }
        })
        //---------------------------------------------------------------------
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
