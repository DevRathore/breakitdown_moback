//
//  NotifyUsViewController.swift
//  BreakItDown
//
//  Created by Dobango on 08/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit
import Firebase

class NotifyUsViewController: UIViewController {

    
    @IBOutlet weak var txtView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //------------ NotifyNew-------------------------------------
        
//        FirebaseApp.configure()

        let refNotify = Database.database().reference().child("NOTIFYUSNEW")
        refNotify.observe(DataEventType.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                for objectData in snapshot.children.allObjects as! [DataSnapshot] {
                    //getting values
                    let objectValue = objectData.value as? [String: AnyObject]
                    let objectTitle: String = objectValue?["title"] as! String
                    
                    let objectContent: String  = objectValue?["content"] as! String
                    
                    //                    let objectSubtitle: [String: AnyObject]  = objectValue?["subtitle"] as! [String : AnyObject]
                    
                    localDataClavicle.notifyUsDetails = objectContent
                    
                    self.txtView.text = localDataClavicle.notifyUsDetails

                    //                    localDataClavicle.clavicleNonOperativeTreatment = objectSubtitle["Non-operative Treatment"] as! String
                    
                    //                    localDataClavicle.clavicleOperativeTreatment = objectSubtitle["Operative Treatment"] as! String
                    
                }
            }
        })
        //---------------------------------------------------------------------

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
