//
//  ThirdView4ViewController.swift
//  BreakItDown
//
//  Created by Dobango on 08/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit
import Firebase

class ThirdView4ViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var injuredBtn: UIButton!
    @IBOutlet weak var painBtn: UIButton!
    @IBOutlet weak var castBtn: UIButton!
    @IBOutlet weak var notifyBtn: UIButton!
    
    
    @IBOutlet weak var injuredContainerView: UIView!
    @IBOutlet weak var painContainerView: UIView!
    @IBOutlet weak var crutchesContainerView: UIView!
    @IBOutlet weak var notifyUsContainerView: UIView!
    
    @IBOutlet weak var menuOptionView: UIView!
    var isSelect: Bool = false
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.


        injuredContainerView.isHidden = false
        painContainerView.isHidden = true
        crutchesContainerView.isHidden = true
        notifyUsContainerView.isHidden = true
        
        injuredBtn.setTitleColor(Color.Blue.Medium, for: .normal)
        injuredBtn.backgroundColor = UIColor.white
        
        painBtn.setTitleColor(UIColor.white, for: .normal)
        painBtn.backgroundColor = Color.Blue.Medium
        
        castBtn.setTitleColor(UIColor.white, for: .normal)
        castBtn.backgroundColor = Color.Blue.Medium
        
        notifyBtn.setTitleColor(UIColor.white, for: .normal)
        notifyBtn.backgroundColor = Color.Blue.Medium
        
        injuredBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 18)
        painBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        castBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        notifyBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        
    }
    
    
    @IBAction func injuredLimbAction(_ sender: Any) {
        
        injuredContainerView.isHidden = false
        painContainerView.isHidden = true
        crutchesContainerView.isHidden = true
        notifyUsContainerView.isHidden = true
        
        injuredBtn.setTitleColor(Color.Blue.Medium, for: .normal)
        injuredBtn.backgroundColor = UIColor.white
        
        painBtn.setTitleColor(UIColor.white, for: .normal)
        painBtn.backgroundColor = Color.Blue.Medium
        
        castBtn.setTitleColor(UIColor.white, for: .normal)
        castBtn.backgroundColor = Color.Blue.Medium
        
        notifyBtn.setTitleColor(UIColor.white, for: .normal)
        notifyBtn.backgroundColor = Color.Blue.Medium
        
        injuredBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 18)
        painBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        castBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        notifyBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)

    }
    
    @IBAction func painAction(_ sender: Any) {
        
        injuredContainerView.isHidden = true
        painContainerView.isHidden = false
        crutchesContainerView.isHidden = true
        notifyUsContainerView.isHidden = true
        
        injuredBtn.setTitleColor(UIColor.white, for: .normal)
        injuredBtn.backgroundColor = Color.Blue.Medium
        
        painBtn.setTitleColor(Color.Blue.Medium, for: .normal)
        painBtn.backgroundColor = UIColor.white
        
        castBtn.setTitleColor(UIColor.white, for: .normal)
        castBtn.backgroundColor = Color.Blue.Medium
        
        notifyBtn.setTitleColor(UIColor.white, for: .normal)
        notifyBtn.backgroundColor = Color.Blue.Medium
        
        injuredBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        painBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 18)
        castBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        notifyBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
    }
    
    @IBAction func castCrutchesAction(_ sender: Any) {
        
        injuredContainerView.isHidden = true
        painContainerView.isHidden = true
        crutchesContainerView.isHidden = false
        notifyUsContainerView.isHidden = true
        
        injuredBtn.setTitleColor(UIColor.white, for: .normal)
        injuredBtn.backgroundColor = Color.Blue.Medium
        
        painBtn.setTitleColor(UIColor.white, for: .normal)
        painBtn.backgroundColor = Color.Blue.Medium
        
        castBtn.setTitleColor(Color.Blue.Medium, for: .normal)
        castBtn.backgroundColor = UIColor.white
        
        notifyBtn.setTitleColor(UIColor.white, for: .normal)
        notifyBtn.backgroundColor = Color.Blue.Medium
        
        injuredBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        painBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        castBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 18)
        notifyBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
    }
    
    @IBAction func notifyUsAction(_ sender: Any) {
        
        injuredContainerView.isHidden = true
        painContainerView.isHidden = true
        crutchesContainerView.isHidden = true
        notifyUsContainerView.isHidden = false
        
        injuredBtn.setTitleColor(UIColor.white, for: .normal)
        injuredBtn.backgroundColor = Color.Blue.Medium
        
        painBtn.setTitleColor(UIColor.white, for: .normal)
        painBtn.backgroundColor = Color.Blue.Medium
        
        castBtn.setTitleColor(UIColor.white, for: .normal)
        castBtn.backgroundColor = Color.Blue.Medium
        
        notifyBtn.setTitleColor(Color.Blue.Medium, for: .normal)
        notifyBtn.backgroundColor = UIColor.white
        
        injuredBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        painBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        castBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 16)
        notifyBtn.titleLabel?.font = UIFont(name: RobotoFontName.RobotoRegular.rawValue, size: 18)
    }
    
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    
    @IBAction func backToHome(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "removeTabbarView"), object: nil)
    }
  
    @IBAction func menuAction(_ sender: Any) {
        
        if isSelect == false
        {
            isSelect = true
            menuOptionView.isHidden = false
        }
        else{
            isSelect = false
            menuOptionView.isHidden = true
        }
    }
    
    @IBAction func faqAction(_ sender: UIButton) {
        
        isSelect = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FaqViewController") as? FaqViewController
        self.present(vc!, animated: false) {
            
        }
        
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        viewFaq.layer.add(transition, forKey: "animation")
        self.viewFaq.isHidden = false
        */
    }
    
    @IBAction func notesAction(_ sender: Any) {
        
        isSelect = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NotesViewController") as? NotesViewController
        self.present(vc!, animated: false) {
            
        }
        
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        notesView.layer.add(transition, forKey: "animation")
        self.notesView.isHidden = false
        
        notesTextView.text = UserDefaults.standard.object(forKey: "notes") as? String
        */
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
