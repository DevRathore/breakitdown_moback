//
//  ThirdView1ViewController.swift
//  BreakItDown
//
//  Created by Dobango on 08/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit
import Firebase


class ThirdView1ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var tableViewList: UITableView!
    var myCareTeamArrayCount = 18

    var addingCell:Bool = false
    var selectedIndex: Int?
    
    var physician = ""
    var residant = ""
    var assistant = ""
    var anesthesiologist = ""
    var nurse = ""
    var manager = ""
    var specialist = ""
    var chaplain = ""
    var instruction = ""

    @IBOutlet weak var menuOptionView: UIView!
    var isSelect: Bool = false
    
    override func viewDidDisappear(_ animated: Bool) {
        
        
        if let name = UserDefaults.standard.object(forKey: "Add Attending Physician's Name") as? String {
            if name == ""
            {
                physician = ""
            }
            else{
                physician = name
            }
        } else {
            physician = ""
        }
        
        
        if let name = UserDefaults.standard.object(forKey: "Add Resident Physician's Name") as? String {
            if name == ""
            {
                residant = ""
            }
            else{
                residant = name
            }
        } else {
            residant = ""
        }
        
        
        if let name = UserDefaults.standard.object(forKey: "Add Physician's Assistant's Name") as? String {
            if name == ""
            {
                assistant = ""
            }
            else{
                assistant = name
            }
        } else {
            assistant = ""
        }
        
        
        if let name = UserDefaults.standard.object(forKey: "Add Anæsthesiologist's Name") as? String {
            if name == ""
            {
                anesthesiologist = ""
            }
            else{
                anesthesiologist = name
            }
        } else {
            anesthesiologist = ""
        }
        
        
        if let name = UserDefaults.standard.object(forKey: "Add Nurse's Name") as? String {
            if name == ""
            {
                nurse = ""
            }
            else{
                nurse = name
            }
        } else {
            nurse = ""
        }
        
        if let name = UserDefaults.standard.object(forKey: "Add Social Worker(s) and Case Manager(S)'s Name") as? String {
            if name == ""
            {
                manager = ""
            }
            else{
                manager = name
            }
        } else {
            manager = ""
        }
        
        if let name = UserDefaults.standard.object(forKey: "Add Child Life Specialist's Name") as? String {
            if name == ""
            {
                specialist = ""
            }
            else{
                specialist = name
            }
        } else {
            specialist = ""
        }
        
        if let name = UserDefaults.standard.object(forKey: "Add Chaplain's Name") as? String {
            if name == ""
            {
                chaplain = ""
            }
            else{
                chaplain = name
            }
        } else {
            chaplain = ""
        }
        
        if let name = UserDefaults.standard.object(forKey: "Add MyChart Instruction's Name") as? String {
            if name == ""
            {
                instruction = ""
            }
            else{
                instruction = name
            }
        } else {
            instruction = ""
        }
        
        
        
        let refInserted = Database.database().reference().child("Inserted")
        let n = Int32(arc4random_uniform(10000000))
        
        refInserted.child((UIDevice.current.identifierForVendor?.uuidString)! + String(n)).setValue(["Physician": physician, "Residant": residant, "Assistant": assistant, "Anesthesiologist": anesthesiologist, "Nurse": nurse, "Manager": manager, "Specialist": specialist, "Chaplain": chaplain, "Instruction": instruction])
 
 
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        
        tableViewList.register(UINib(nibName: CellIdentifiers.anatomyCell1, bundle: nil), forCellReuseIdentifier: CellIdentifiers.anatomyCell1)
        tableViewList.register(UINib(nibName: CellIdentifiers.textFieldCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.textFieldCell)

        tableViewList.estimatedRowHeight = 100
        tableViewList.rowHeight = UITableViewAutomaticDimension

        //------------ CareTeamNew-------------------------------------
        
//        FirebaseApp.configure()

        let refCareTeam = Database.database().reference().child("CARETEAMNEW")
        refCareTeam.observe(DataEventType.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                for objectData in snapshot.children.allObjects as! [DataSnapshot] {
                    //getting values
                    let objectValue = objectData.value as? [String: AnyObject]
                    
                    let objectContent1: String  = objectValue?["anesthesiologist"] as! String
                    let objectContent2: String  = objectValue?["attendingPhysician"] as! String
                    let objectContent3: String  = objectValue?["chaplain"] as! String
                    let objectContent4: String  = objectValue?["childLifeSpecialist"] as! String
                    
                    let objectContent5: String  = objectValue?["myChartInstruction"] as! String
                    let objectContent6: String  = objectValue?["nurse"] as! String
                    let objectContent7: String  = objectValue?["physicanAssist"] as! String
                    let objectContent8: String  = objectValue?["residentPhysician"] as! String
                    let objectContent9: String  = objectValue?["socialWorker"] as! String
                    
                    
                    localDataClavicle.anesthesiologistDetails = objectContent1
                    localDataClavicle.attendingPhysicianDetails = objectContent2
                    localDataClavicle.chaplainDetails = objectContent3
                    localDataClavicle.childLifeSpecialistDetails = objectContent4
                    
                    localDataClavicle.myChartInstructionDetails = objectContent5
                    localDataClavicle.nurseDetails = objectContent6
                    localDataClavicle.physicianAttendentDetails = objectContent7
                    localDataClavicle.residentPhysicianDetails = objectContent8
                    
                    localDataClavicle.socialWorkerCaseManagerDetails = objectContent9
                    
                    self.reloadContents()

                }
            }
        })
        //---------------------------------------------------------------------
        
        self.reloadContents()

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return myCareTeamArrayCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var celll:UITableViewCell?

        let cellTxtField = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.textFieldCell, for: indexPath) as! TextFieldTableViewCell

        if indexPath.row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
            cell.selectionStyle = .none
            cell.updateCell("Attending Physician", color1: Color.Blue.Medium, color2: Color.Blue.Light)
            if addingCell && selectedIndex == indexPath.row {
                cell.data.isHidden = false
                cell.data.text = localDataClavicle.attendingPhysicianDetails
                cell.contentView.setNeedsLayout()
            } else {
                cell.data.isHidden = true
                cell.data.text = ""
                cell.contentView.setNeedsLayout()
            }
            
            return cell
        }
        else if indexPath.row == 1 {
            
            cellTxtField.selectionStyle = .none
            cellTxtField.textField.tag = indexPath.row
            cellTxtField.textField.delegate = self
            cellTxtField.updateCell("Add Attending Physician's Name")
//            cellTxtField.configure(cellTxtField.textField.placeholder!)

            cellTxtField.textField.text = UserDefaults.standard.object(forKey: "Add Attending Physician's Name") as? String
            
            return cellTxtField
        }
        else if indexPath.row == 2 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
            cell.selectionStyle = .none
            cell.updateCell("Resident Physician", color1: Color.Blue.Medium, color2: Color.Blue.Light)
            if addingCell && selectedIndex == indexPath.row {
                cell.data.isHidden = false
                cell.data.text = localDataClavicle.residentPhysicianDetails
                cell.contentView.setNeedsLayout()
            } else {
                cell.data.isHidden = true
                cell.data.text = ""
                cell.contentView.setNeedsLayout()
            }
            
            return cell
        }
        else if indexPath.row == 3 {
            
            cellTxtField.selectionStyle = .none
            cellTxtField.textField.tag = indexPath.row
            cellTxtField.textField.delegate = self
            cellTxtField.updateCell("Add Resident Physician's Name")
//            cellTxtField.configure(cellTxtField.textField.placeholder!)

            cellTxtField.textField.text = UserDefaults.standard.object(forKey: "Add Resident Physician's Name") as? String

            return cellTxtField
        }
        else if indexPath.row == 4 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
            cell.selectionStyle = .none
            cell.updateCell("Physician's Assistant", color1: Color.Blue.Medium, color2: Color.Blue.Light)
            if addingCell && selectedIndex == indexPath.row {
                cell.data.isHidden = false
                cell.data.text = localDataClavicle.physicianAttendentDetails
                cell.contentView.setNeedsLayout()
            } else {
                cell.data.isHidden = true
                cell.data.text = ""
                cell.contentView.setNeedsLayout()
            }
            
            return cell
        }
        else if indexPath.row == 5 {
            
            cellTxtField.selectionStyle = .none
            cellTxtField.textField.tag = indexPath.row
            cellTxtField.textField.delegate = self
            cellTxtField.updateCell("Add Physician's Assistant's Name")
//            cellTxtField.configure(cellTxtField.textField.placeholder!)

            cellTxtField.textField.text = UserDefaults.standard.object(forKey: "Add Physician's Assistant's Name") as? String

            return cellTxtField
        }
        else if indexPath.row == 6 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
            cell.selectionStyle = .none
            cell.updateCell("Anæsthesiologist", color1: Color.Blue.Medium, color2: Color.Blue.Light)
            if addingCell && selectedIndex == indexPath.row {
                cell.data.isHidden = false
                cell.data.text = localDataClavicle.anesthesiologistDetails
                cell.contentView.setNeedsLayout()
            } else {
                cell.data.isHidden = true
                cell.data.text = ""
                cell.contentView.setNeedsLayout()
            }
            
            return cell
        }
        else if indexPath.row == 7 {
            
            cellTxtField.selectionStyle = .none
            cellTxtField.textField.tag = indexPath.row
            cellTxtField.textField.delegate = self
            cellTxtField.updateCell("Add Anæsthesiologist's Name")
//            cellTxtField.configure(cellTxtField.textField.placeholder!)

            cellTxtField.textField.text = UserDefaults.standard.object(forKey: "Add Anæsthesiologist's Name") as? String

            
            return cellTxtField
        }
        else if indexPath.row == 8 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
            cell.selectionStyle = .none
            cell.updateCell("Nurse", color1: Color.Blue.Medium, color2: Color.Blue.Light)
            if addingCell && selectedIndex == indexPath.row {
                cell.data.isHidden = false
                cell.data.text = localDataClavicle.nurseDetails
                cell.contentView.setNeedsLayout()
            } else {
                cell.data.isHidden = true
                cell.data.text = ""
                cell.contentView.setNeedsLayout()
            }
            
            return cell
        }
        else if indexPath.row == 9 {
            
            cellTxtField.selectionStyle = .none
            cellTxtField.textField.tag = indexPath.row
            cellTxtField.textField.delegate = self
            cellTxtField.updateCell("Add Nurse's Name")
//            cellTxtField.configure(cellTxtField.textField.placeholder!)

            cellTxtField.textField.text = UserDefaults.standard.object(forKey: "Add Nurse's Name") as? String

            return cellTxtField
        }
        else if indexPath.row == 10 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
            cell.selectionStyle = .none
            cell.updateCell("Social Worker(s) and Case Manager(S)", color1: Color.Blue.Medium, color2: Color.Blue.Light)
            if addingCell && selectedIndex == indexPath.row {
                cell.data.isHidden = false
                cell.data.text = localDataClavicle.socialWorkerCaseManagerDetails
                cell.contentView.setNeedsLayout()
            } else {
                cell.data.isHidden = true
                cell.data.text = ""
                cell.contentView.setNeedsLayout()
            }
            
            return cell
        }
        else if indexPath.row == 11 {
            
            cellTxtField.selectionStyle = .none
            cellTxtField.textField.tag = indexPath.row
            cellTxtField.textField.delegate = self
            cellTxtField.updateCell("Add Social Worker(s) and Case Manager(S)'s Name")
//            cellTxtField.configure(cellTxtField.textField.placeholder!)

            cellTxtField.textField.text = UserDefaults.standard.object(forKey: "Add Social Worker(s) and Case Manager(S)'s Name") as? String

            return cellTxtField
        }
        else if indexPath.row == 12 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
            cell.selectionStyle = .none
            cell.updateCell("Child Life Specialist", color1: Color.Blue.Medium, color2: Color.Blue.Light)
            if addingCell && selectedIndex == indexPath.row {
                cell.data.isHidden = false
                cell.data.text = localDataClavicle.childLifeSpecialistDetails
                cell.contentView.setNeedsLayout()
            } else {
                cell.data.isHidden = true
                cell.data.text = ""
                cell.contentView.setNeedsLayout()
            }
            
            return cell
        }
        else if indexPath.row == 13 {
            
            cellTxtField.selectionStyle = .none
            cellTxtField.textField.tag = indexPath.row
            cellTxtField.textField.delegate = self
            cellTxtField.updateCell("Add Child Life Specialist's Name")
//            cellTxtField.configure(cellTxtField.textField.placeholder!)

            cellTxtField.textField.text = UserDefaults.standard.object(forKey: "Add Child Life Specialist's Name") as? String

            return cellTxtField
        }
        else if indexPath.row == 14 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
            cell.selectionStyle = .none
            cell.updateCell("Chaplain", color1: Color.Blue.Medium, color2: Color.Blue.Light)
            if addingCell && selectedIndex == indexPath.row {
                cell.data.isHidden = false
                cell.data.text = localDataClavicle.chaplainDetails
                cell.contentView.setNeedsLayout()
            } else {
                cell.data.isHidden = true
                cell.data.text = ""
                cell.contentView.setNeedsLayout()
            }
            
            return cell
        }
        else if indexPath.row == 15 {
            
            cellTxtField.selectionStyle = .none
            cellTxtField.textField.tag = indexPath.row
            cellTxtField.textField.delegate = self
            cellTxtField.updateCell("Add Chaplain's Name")
//            cellTxtField.configure(cellTxtField.textField.placeholder!)

            cellTxtField.textField.text = UserDefaults.standard.object(forKey: "Add Chaplain's Name") as? String

            return cellTxtField
        }
        else if indexPath.row == 16 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
            cell.selectionStyle = .none
            cell.updateCell("MyChart Instructions", color1: Color.Blue.Medium, color2: Color.Blue.Light)
            if addingCell && selectedIndex == indexPath.row {
                cell.data.isHidden = false
                cell.data.text = localDataClavicle.myChartInstructionDetails
                cell.contentView.setNeedsLayout()
            } else {
                cell.data.isHidden = true
                cell.data.text = ""
                cell.contentView.setNeedsLayout()
            }
            
            return cell
        }
        else if indexPath.row == 17 {
            
            cellTxtField.selectionStyle = .none
            cellTxtField.textField.tag = indexPath.row
            cellTxtField.textField.delegate = self
            cellTxtField.updateCell("Add MyChart Instruction's Name")
//            cellTxtField.configure(cellTxtField.textField.placeholder!)

            cellTxtField.textField.text = UserDefaults.standard.object(forKey: "Add MyChart Instruction's Name") as? String

            return cellTxtField
        }
        
        return celll!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UIDevice.current.userInterfaceIdiom != .pad {
            
            if addingCell == false
            {
                addingCell = true
            }
            else{
                addingCell = false
            }
            tableView.reloadData()
            selectedIndex = indexPath.row
            scrollTableViewToRow(indexPath.row)
            
            self.view.endEditing(true)
            
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        }
    }
    
    func scrollTableViewToRow(_ row:Int) {
        let seconds = 0.1
        let delay = seconds * Double(NSEC_PER_SEC)  // nanoseconds per seconds
        let dispatchTime = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.tableViewList.scrollToRow(at: IndexPath(row: row, section: 0), at: UITableViewScrollPosition.top, animated: true)
        })
    }
    
    func reloadContents() {
        tableViewList.delegate = self
        tableViewList.dataSource = self
        tableViewList.reloadData()
    }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
    
    
    @IBAction func backToHome(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "removeTabbarView"), object: nil)
    }
   

    @IBAction func menuAction(_ sender: Any) {
        
        if isSelect == false
        {
            isSelect = true
            menuOptionView.isHidden = false
        }
        else{
            isSelect = false
            menuOptionView.isHidden = true
        }
    }
    
    @IBAction func faqAction(_ sender: UIButton) {
        
        isSelect = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FaqViewController") as? FaqViewController
        self.present(vc!, animated: false) {
            
        }
        
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        viewFaq.layer.add(transition, forKey: "animation")
        self.viewFaq.isHidden = false
        */
    }
    
    @IBAction func notesAction(_ sender: Any) {
        
        isSelect = false
        menuOptionView.isHidden = true
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NotesViewController") as? NotesViewController
        self.present(vc!, animated: false) {
            
        }

        
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        notesView.layer.add(transition, forKey: "animation")
        self.notesView.isHidden = false
        
        notesTextView.text = UserDefaults.standard.object(forKey: "notes") as? String
        */
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidChange(_ textField: UITextField) {
     
//        tableViewList.setContentOffset(.zero, animated: true)
        scrollTableViewToRow(0)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        // return NO to not change text
        
        if textField.tag == 1 || textField.tag == 3 || textField.tag == 5 {
            scrollTableViewToRow(0)
        }
        else if textField.tag == 7{
            scrollTableViewToRow(2)
        }
        else if textField.tag == 9{
            scrollTableViewToRow(2)
        }
        else if textField.tag == 11{
            scrollTableViewToRow(4)
        }
        else if textField.tag == 13{
            scrollTableViewToRow(13)
        }
        else if textField.tag == 15{
            scrollTableViewToRow(15)
        }
        else if textField.tag == 17{
            scrollTableViewToRow(17)
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if !(textField.tag == 1 || textField.tag == 3 || textField.tag == 5)
        {
            if textField.tag == 15 || textField.tag == 17
            {
                self.view.frame = CGRect(x: 0, y: -200, width: self.view.frame.width, height: self.view.frame.height)
            }
            else{
             
                self.view.frame = CGRect(x: 0, y: -150, width: self.view.frame.width, height: self.view.frame.height)
            }
            
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        //        searchBooks(textField)
        textField.resignFirstResponder()
        
        if textField.tag == 1 {
            UserDefaults.standard.set(textField.text!, forKey: "Add Attending Physician's Name")

        }
        else if textField.tag == 3 {
            UserDefaults.standard.set(textField.text!, forKey: "Add Resident Physician's Name")

        }
        else if textField.tag == 5 {
            UserDefaults.standard.set(textField.text!, forKey: "Add Physician's Assistant's Name")

        }
        else if textField.tag == 7 {
            UserDefaults.standard.set(textField.text!, forKey: "Add Anæsthesiologist's Name")

        }
        else if textField.tag == 9 {
            UserDefaults.standard.set(textField.text!, forKey: "Add Nurse's Name")

        }
        else if textField.tag == 11 {
            UserDefaults.standard.set(textField.text!, forKey: "Add Social Worker(s) and Case Manager(S)'s Name")

        }
        else if textField.tag == 13 {
            UserDefaults.standard.set(textField.text!, forKey: "Add Child Life Specialist's Name")

        }
        else if textField.tag == 15 {
            UserDefaults.standard.set(textField.text!, forKey: "Add Chaplain's Name")

        }
        else if textField.tag == 17 {
            UserDefaults.standard.set(textField.text!, forKey: "Add MyChart Instruction's Name")

        }
        
        

        
        //        self.view.endEditing(true)
        //
        //        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        /*
        let refInserted = Database.database().reference().child("Inserted")
        let n = Int32(arc4random_uniform(10000000))
        refInserted.child((UIDevice.current.identifierForVendor?.uuidString)! + String(n)).setValue([(textField.tag == 1 ? "Physician Name" : textField.tag == 2 ? "Physician Resident" : textField.tag == 3 ? "Physician Assitant" : textField.tag == 4 ? "Anesthesiologist" : textField.tag == 5 ? "Nurse" : textField.tag == 6 ? "Social Worker" : textField.tag == 7 ? "Child Life Specialist" : textField.tag == 8 ? "Chaplain" : textField.tag == 9 ? "MyChart Instruction" : "Physician"): textField.text])
        */
 
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        self.view.endEditing(true)
        
        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        return true
    }
}





