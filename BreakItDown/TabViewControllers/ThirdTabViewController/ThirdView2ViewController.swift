//
//  ThirdView2ViewController.swift
//  BreakItDown
//
//  Created by Dobango on 08/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit

class ThirdView2ViewController: UIViewController, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var tableViewList: UITableView!
    
    @IBOutlet weak var tableViewListOption: UITableView!
    
    
    @IBOutlet weak var menuOptionView: UIView!
    var isSelect: Bool = false
    
    var surgeryArrayCount = 2

    var addingCell:Bool = false
    var selectedIndex: Int?

    var arrayWithOption = ["  Clavicle: Non-Operative", "  Clavicle: Operative"]

    override func viewDidLoad() {
        super.viewDidLoad()

        tableViewList.register(UINib(nibName: CellIdentifiers.anatomyCell1, bundle: nil), forCellReuseIdentifier: CellIdentifiers.anatomyCell1)

        tableViewListOption.register(UINib(nibName: CellIdentifiers.selectionCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.selectionCell)

        tableViewList.estimatedRowHeight = 100
        tableViewList.rowHeight = UITableViewAutomaticDimension
        
        self.reloadContents()
        self.reloadDropDownContents()

    }

    func reloadContents() {
        tableViewList.delegate = self
        tableViewList.dataSource = self
        tableViewList.reloadData()
    }
    
    func reloadDropDownContents() {
        tableViewListOption.delegate = self
        tableViewListOption.dataSource = self
        tableViewListOption.reloadData()
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            return arrayWithOption.count
        }
        else{
            return surgeryArrayCount
        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var celll:UITableViewCell?
        
        if tableView.tag == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.selectionCell, for: indexPath) as! SelectionTableViewCell
            cell.selectionStyle = .none
            cell.updateOptionCell(arrayWithOption[indexPath.row], color1: UIColor.clear, color2: Color.Blue.Light)

            return cell
        }
        else if tableView.tag == 2
        {
            if indexPath.row == 0  {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.label.textAlignment = .left
                cell.updateCell("  My Recovery:", color1: Color.Blue.Medium, color2: Color.Blue.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.clavicleNonOperativeTreatment
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                
                return cell
            }
            else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.anatomyCell1, for: indexPath) as! AnatomyTableViewCell1
                cell.selectionStyle = .none
                cell.label.textAlignment = .left
                cell.updateCell("  Care Tips:", color1: Color.Blue.Medium, color2: Color.Blue.Light)
                if addingCell && selectedIndex == indexPath.row {
                    cell.data.isHidden = false
                    cell.data.text = localDataClavicle.clavicleNonOperativeTreatment
                    cell.contentView.setNeedsLayout()
                } else {
                    cell.data.isHidden = true
                    cell.data.text = ""
                    cell.contentView.setNeedsLayout()
                }
                return cell
            }
        }
        
        return celll!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UIDevice.current.userInterfaceIdiom != .pad {
            
            if tableView.tag == 1
            {
                btnMenu.isSelected = false
                tableViewListOption.isHidden = true
                btnMenu.setTitle(arrayWithOption[indexPath.row], for: UIControlState.normal)
            }
            else if tableView.tag == 2{
                if indexPath.row == 0 || indexPath.row == 1
                {
                    if addingCell == false
                    {
                        addingCell = true
                        //                            self.addDetailCellIntoView(indexPath)
                    }
                    else{
                        addingCell = false
                        //                            self.removeDetailCellIntoView(indexPath)
                    }
                    tableView.reloadData()
                    selectedIndex = indexPath.row
                    scrollTableViewToRow(indexPath.row)
                }
            }
        }
    }
    
    func scrollTableViewToRow(_ row:Int) {
        let seconds = 0.1
        let delay = seconds * Double(NSEC_PER_SEC)  // nanoseconds per seconds
        let dispatchTime = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.tableViewList.scrollToRow(at: IndexPath(row: row, section: 0), at: UITableViewScrollPosition.top, animated: false)
        })
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController!.popViewController(animated: true)
    }
    
    @IBAction func backToHome(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "removeTabbarView"), object: nil)
    }
    
    @IBAction func menuAction(_ sender: Any) {
        
        if isSelect == false
        {
            isSelect = true
            menuOptionView.isHidden = false
        }
        else{
            isSelect = false
            menuOptionView.isHidden = true
        }
    }
 
    
    @IBAction func faqAction(_ sender: UIButton) {
        
        isSelect = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FaqViewController") as? FaqViewController
        self.present(vc!, animated: false) {
            
        }
        
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        viewFaq.layer.add(transition, forKey: "animation")
        self.viewFaq.isHidden = false
        */
        
    }
    
    @IBAction func notesAction(_ sender: Any) {
        
        isSelect = false
        menuOptionView.isHidden = true
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NotesViewController") as? NotesViewController
        self.present(vc!, animated: false) {
            
        }

        
        /*
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.fillMode = kCAFillModeForwards
        transition.duration = 0.5
        transition.subtype = kCATransitionFromTop
        notesView.layer.add(transition, forKey: "animation")
        self.notesView.isHidden = false
        
        notesTextView.text = UserDefaults.standard.object(forKey: "notes") as? String
        */
    }
    
    @IBAction func wheelAction(_ sender: UIButton) {
        
        if sender.isSelected == false {
            sender.isSelected = true
            tableViewListOption.isHidden = false
        }
        else{
            sender.isSelected = false
            tableViewListOption.isHidden = true
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
