//
//  ViewController.swift
//  BreakItDown
//
//  Created by Dobango on 05/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let hhTabBarView = HHTabBarView.shared
    
    var indexNumber: Int = 0
    
    
    //Keeping reference of iOS default UITabBarController.
    let referenceUITabBarController = HHTabBarView.shared.referenceUITabBarController
    
    //2
    func setupReferenceUITabBarController() {
        
        //Creating a storyboard reference
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        //First and Second Tab ViewControllers will be taken from the UIStoryBoard
        //Creating navigation controller for navigation inside the first tab.
        
        //First and Second Tab ViewControllers will be taken from the UIStoryBoard
        //Creating navigation controller for navigation inside the first tab.
        let navigationController1: UINavigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "FirstViewController"))
        
        //First and Second Tab ViewControllers will be taken from the UIStoryBoard
        //Creating navigation controller for navigation inside the first tab.
        let navigationController2: UINavigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "SecondViewController"))
        
        //First and Second Tab ViewControllers will be taken from the UIStoryBoard
        //Creating navigation controller for navigation inside the first tab.
        let navigationController3: UINavigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "ThirdViewController"))
        
        //First and Second Tab ViewControllers will be taken from the UIStoryBoard
        //Creating navigation controller for navigation inside the first tab.
        let navigationController4: UINavigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "ForthViewController"))
        
        //First and Second Tab ViewControllers will be taken from the UIStoryBoard
        //Creating navigation controller for navigation inside the first tab.
        let navigationController5: UINavigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "FiveViewController"))
        
        
        
        /*
         let firstViewController = FirstViewController()
         firstViewController.title = "First"
         firstViewController.view.backgroundColor = .white
         let firstNavigationController: UINavigationController = UINavigationController(rootViewController: firstViewController)
         
         let secondViewController = SecondViewController()
         secondViewController.title = "Second"
         secondViewController.view.backgroundColor = .white
         let secondNavigationController: UINavigationController = UINavigationController(rootViewController: secondViewController)
         
         //Third, Fourth and Fifth will be created runtime.
         let sofaViewController = ThirdViewController()
         sofaViewController.title = "Sofa"
         sofaViewController.view.backgroundColor = .white
         let sofaNavigationController: UINavigationController = UINavigationController(rootViewController: sofaViewController)
         
         let targetViewController = ForthViewController()
         targetViewController.title = "Target"
         targetViewController.view.backgroundColor = .white
         let targetNavigationController: UINavigationController = UINavigationController(rootViewController: targetViewController)
         
         let umbrellaViewController = FiveViewController()
         umbrellaViewController.title = "Umbrella"
         umbrellaViewController.view.backgroundColor = .white
         let umbrellaNavigationController: UINavigationController = UINavigationController(rootViewController: umbrellaViewController)
         */
        
        //Update referenced TabbarController with your viewcontrollers
        referenceUITabBarController.setViewControllers([navigationController1, navigationController2, navigationController3, navigationController4, navigationController5], animated: false)
        
    }
    
    //3
    func setupHHTabBarView() {
        
        //Default & Selected Background Color
        let defaultTabColor = Color.Blue.Light
        //UIColor.orange.withAlphaComponent(0.8)
        
        let selectedTabColor = UIColor(red: 125/255, green: 180/255, blue: 45/255, alpha: 1.0)
        let tabFont = UIFont.init(name: "Helvetica-Light", size: 12.0)
        let spacing: CGFloat = 3.0
        
        //Create Custom Tabs
        //Note: As tabs are subclassed of UIButton so you can modify it as much as possible.
        
        let titles = ["", "", "", "", ""]
        let icons = [UIImage(named: "Asset 3")!, UIImage(named: "Asset 5")!, UIImage(named: "Asset 1")!, UIImage(named: "Asset 4")!, UIImage(named: "Asset 2")!]
        var tabs = [HHTabButton]()
        
        for index in 0...4 {
            let tab = HHTabButton(withTitle: titles[index], tabImage: icons[index], index: index)
            tab.titleLabel?.font = tabFont
            
            if index == 0
            {
                tab.setHHTabBackgroundColor(color: defaultTabColor, forState: .normal)
                tab.setHHTabBackgroundColor(color: Color.Green.Medium, forState: .selected)
            }
            else if index == 1
            {
                tab.setHHTabBackgroundColor(color: defaultTabColor, forState: .normal)
                tab.setHHTabBackgroundColor(color: Color.Orange.Medium, forState: .selected)
            }
            else if index == 2
            {
                tab.setHHTabBackgroundColor(color: defaultTabColor, forState: .normal)
                tab.setHHTabBackgroundColor(color: Color.Blue.Medium, forState: .selected)
            }
            else if index == 3
            {
                tab.setHHTabBackgroundColor(color: defaultTabColor, forState: .normal)
                tab.setHHTabBackgroundColor(color: Color.Teal.Medium, forState: .selected)
            }
            else if index == 4
            {
                tab.setHHTabBackgroundColor(color: defaultTabColor, forState: .normal)
                tab.setHHTabBackgroundColor(color: Color.Purple.Medium, forState: .selected)
            }
            
            
            tab.imageToTitleSpacing = spacing
            tab.imageVerticalAlignment = .top
            tab.imageHorizontalAlignment = .center
            tabs.append(tab)
        }
        
        //Set HHTabBarView position.
        hhTabBarView.tabBarViewPosition = .bottom
        
        //Set this value according to your UI requirements.
        hhTabBarView.tabBarViewTopPositionValue = 44
        
        //Set Default Index for HHTabBarView.
        hhTabBarView.tabBarTabs = tabs
        
        // To modify badge label.
        // Note: You should only modify badgeLabel after assigning tabs array.
        // Example:
        //t1.badgeLabel?.backgroundColor = .white
        //t1.badgeLabel?.textColor = selectedTabColor
        //t1.badgeLabel?.font = UIFont.boldSystemFont(ofSize: 20.0)
        
        //Handle Tab Change Event
        hhTabBarView.defaultIndex = indexNumber
        
        //Show Animation on Switching Tabs
        hhTabBarView.tabChangeAnimationType = .none
        
        //Handle Tab Changes
        hhTabBarView.onTabTapped = { (tabIndex) in
            print("Selected Tab Index:\(tabIndex)")
        }
        
}
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.removeTabbarView), name: NSNotification.Name(rawValue: "removeTabbarView"), object: nil)
        
        //Setup HHTabBarView
        setupReferenceUITabBarController()
        setupHHTabBarView()
        self.view.addSubview(referenceUITabBarController.view)
        
        if (Device.IS_IPHONE_X){
            //iPhone X
            
            let iv = UIView(frame: CGRect(x: 120, y: 40, width: self.view.frame.width - 240, height: 44))
//            iv.backgroundColor = UIColor.orange
            
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: iv.frame.size.width, height: iv.frame.size.height))
            
            let image = UIImage(named: "break it down")
            imageView.image = image
            imageView.contentMode = .scaleAspectFit
            iv.addSubview(imageView)
            self.view.addSubview(iv)
        }
        else{
            let iv = UIView(frame: CGRect(x: 120, y: 20, width: self.view.frame.width - 240, height: 44))
//            iv.backgroundColor = UIColor.orange
            
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: iv.frame.size.width, height: iv.frame.size.height))
            
            let image = UIImage(named: "break it down")
            imageView.image = image
            imageView.contentMode = .scaleAspectFit
            iv.addSubview(imageView)
            self.view.addSubview(iv)
        }
        
        
        UINavigationBar.appearance().barTintColor = Color.Blue.Light
    }
    
    @objc func removeTabbarView() {
        self.dismiss(animated: false) {
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

