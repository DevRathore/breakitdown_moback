//
//  AnatomyTableViewCell1.swift
//  BreakItDown
//
//  Created by Dobango on 09/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit

class AnatomyTableViewCell1: UITableViewCell {

    
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var data: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(_ text:String, color1: UIColor, color2: UIColor){
//        label.font = UIFont.boldSystemFont(ofSize: 15.0)
        label.backgroundColor = color1
        data.backgroundColor = color2
        label.text = text;
        
    }
    
}
