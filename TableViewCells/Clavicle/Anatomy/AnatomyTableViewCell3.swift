//
//  AnatomyTableViewCell3.swift
//  BreakItDown
//
//  Created by Dobango on 09/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit

class AnatomyTableViewCell3: UITableViewCell {

    @IBOutlet weak var labelSummary: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(_ content: String){
        labelSummary.text = content
    }
}
