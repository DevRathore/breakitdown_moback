//
//  AddressPhoneTableViewCell.swift
//  BreakItDown
//
//  Created by Dobango on 19/11/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit

class AddressPhoneTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelSummary: UILabel!
    @IBOutlet weak var btnPhoneCall: UIButton!
    
    var phoneString = ""
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(_ content: String){
//        labelSummary.text = content
        
        var str = content
        str = str.components(separatedBy: "\n(")[0]
        
        labelSummary.text = str
        
        if let range = content.range(of: "(") {
            let phone = content[range.upperBound...]
            print(phone)
            
            let delCharSet = NSCharacterSet(charactersIn: ") ")
            
            let replaced1 = phone.replacingOccurrences(of: ")", with: "")
            
//            let s1Del = phone.trimmingCharacters(in: delCharSet as CharacterSet)
//            print(s1Del) //->aString

            let replaced2 = replaced1.replacingOccurrences(of: "-", with: " ")

//            replaced2 = "+" + replaced2
            
            phoneString = replaced2
            
            phoneString = phoneString.replacingOccurrences(of: " ", with: "")

            print(phoneString)
            print(replaced2)
            
//            btnPhoneCall.setTitle(phoneString, for: .normal)
            btnPhoneCall.setTitle("(" + phone, for: .normal)

            /*
            var str = content
            str = str.componentsSeparatedByString("\n")[0]
            */
            

        }
    }
    
    @IBAction func callAction(_ sender: UIButton) {
        if let phoneCallURL = URL(string: "tel://\(phoneString)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    
}
