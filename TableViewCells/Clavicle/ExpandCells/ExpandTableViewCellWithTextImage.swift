//
//  ExpandTableViewCellWithTextImage.swift
//  BreakItDown
//
//  Created by Dobango on 09/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit

class ExpandTableViewCellWithTextImage: UITableViewCell {

    @IBOutlet weak var labelSummary: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(_ text:String){
        //        label.font = UIFont.boldSystemFont(ofSize: 15.0)
        labelSummary.text = text;
    }
}
