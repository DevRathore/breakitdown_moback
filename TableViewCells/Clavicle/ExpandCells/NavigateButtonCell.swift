//
//  NavigateButtonCell.swift
//  BackMeUp
//
//  Created by Hing Huynh on 7/24/15.
//  Copyright (c) 2015 MoBack Inc. All rights reserved.
//

import UIKit

class NavigateButtonCell: UITableViewCell {

    @IBOutlet weak var button: NavigationButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
