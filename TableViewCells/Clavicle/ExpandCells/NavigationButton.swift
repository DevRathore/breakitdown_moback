//
//  NavigationButton.swift
//  BackMeUp
//
//  Created by Lucus Huang on 7/22/15.
//  Copyright (c) 2015 MoBack Inc. All rights reserved.
//

import UIKit
import CoreLocation

class NavigationButton: UIButton, CLLocationManagerDelegate, UIAlertViewDelegate {
    
    fileprivate lazy var alertView: UIAlertView = {
        let alertView = UIAlertView(title: "Redirect request", message: "Are you sure you want to open up the Apple Map to UCSF facility?", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Accept")
        return alertView
    }()
    
    @available(iOS 8.0, *)
    fileprivate lazy var alert: UIAlertController? = {
        let alertController = UIAlertController(title: "Redirect request", message: "Are you sure you want to open up the Apple Map to UCSF facility?", preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) {
            (alertAction: UIAlertAction!) -> Void in
            
            self.alert?.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(cancelAction)
        
        let acceptAction = UIAlertAction(title: "Accept", style: UIAlertActionStyle.default) {
            (alertAction: UIAlertAction) -> Void in
            
            let mapLocationUrlString = "http://maps.apple.com/?daddr=37.764833,-122.390322"
            let mapLocationUrl = URL(string: mapLocationUrlString)
            if UIApplication.shared.canOpenURL(mapLocationUrl!) {
                UIApplication.shared.openURL(mapLocationUrl!)
            }
            else {
                print("Can't open \(mapLocationUrlString)")
            }
        }
        alertController.addAction(acceptAction)
        
        return alertController
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addTarget(self, action: #selector(NavigationButton.navigationButtonPressed), for: UIControlEvents.touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.addTarget(self, action: #selector(NavigationButton.navigationButtonPressed), for: UIControlEvents.touchUpInside)
    }
    
    @objc func navigationButtonPressed() {
        
        if NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1 {
            self.alertView.show()
        }
        else {
            if #available(iOS 8.0, *) {
//                activeController.present(self.alert!, animated: true, completion: nil)
                self.alertView.show()
            } else {
                // Fallback on earlier versions
            }
        }
        /*
        let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
        if let activeController = navigationController!.visibleViewController {
            
            if NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1 {
                self.alertView.show()
            }
            else {
                if #available(iOS 8.0, *) {
                    activeController.present(self.alert!, animated: true, completion: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        else {
            if NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1 {
                self.alertView.show()
            }
            else {
                if #available(iOS 8.0, *) {
                    UIApplication.shared.keyWindow?.rootViewController?.present(self.alert!, animated: true, completion: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        */
    }
    
    // MAKR: - UIAlertViewDelegate
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 1 {
            let mapLocationUrlString = "http://maps.apple.com/?daddr=37.764833,-122.390322"
            let mapLocationUrl = URL(string: mapLocationUrlString)
            if UIApplication.shared.canOpenURL(mapLocationUrl!) {
                UIApplication.shared.openURL(mapLocationUrl!)
            }
            else {
                print("Can't open \(mapLocationUrlString)")
            }
        }
    }
}
