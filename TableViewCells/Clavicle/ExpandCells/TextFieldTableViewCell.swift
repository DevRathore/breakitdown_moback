//
//  TextFieldTableViewCell.swift
//  BreakItDown
//
//  Created by Dobango on 12/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(_ text:String){
        if let name = UserDefaults.standard.object(forKey: text) as? String {
            self.textField.text = name
        } else {
            textField.placeholder = text
        }
    }
    
    func updateCell(_ text:String){
        //        label.font = UIFont.boldSystemFont(ofSize: 15.0)
        textField.placeholder = text
    }
}

