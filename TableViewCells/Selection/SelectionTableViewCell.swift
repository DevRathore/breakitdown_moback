//
//  SelectionTableViewCell.swift
//  BreakItDown
//
//  Created by Dobango on 15/10/18.
//  Copyright © 2018 Dobango. All rights reserved.
//

import UIKit

class SelectionTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(_ text:String){
        //        label.font = UIFont.boldSystemFont(ofSize: 15.0)
        titleLabel.text = text;
    }
    
    func updateOptionCell(_ text:String, color1: UIColor, color2: UIColor){
        //        label.font = UIFont.boldSystemFont(ofSize: 15.0)
        titleLabel.backgroundColor = color1
        titleLabel.textColor = color2
        self.backgroundColor = UIColor.white
        titleLabel.text = text;
    }
}
